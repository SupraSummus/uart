--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:	16:51:58 12/16/2014
-- Design Name:	
-- Module Name:	Z:/pul/zadanie3-uart/test_uart_loop.vhd
-- Project Name:  zadanie3-serial
-- Target Device:  
-- Tool versions:  
-- Description:	
-- 
-- VHDL Test Bench Created by ISE for module: uart_loop
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY test_uart_loop_continous_transmission IS
END test_uart_loop_continous_transmission;
 
ARCHITECTURE behavior OF test_uart_loop_continous_transmission IS 
 
	 -- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT uart_loop
	generic(
			clock_frequency: natural;
			baud_rate: natural;
			nbit: natural := 8
		);
		PORT(
			clock : IN  std_logic;
			comm_in : in  std_logic;
			comm_out : out  std_logic
		);
	END COMPONENT;
	
	COMPONENT uart
	generic(
			clock_frequency: natural;
			baud_rate: natural;
			nbit: natural := 8
		);
		PORT(
			clock: in std_logic;
		
			input: in std_logic_vector(nbit - 1 downto 0);
			tx_strobe: in std_logic;
			tx_ready: out std_logic;
			comm_out: out std_logic;
			
			output: out std_logic_vector(nbit - 1 downto 0);
			rx_strobe: in std_logic;
			rx_ready: out std_logic;
			comm_in: in std_logic
		);
	END COMPONENT;
	 

	--Inputs
	signal clock : std_logic;
	signal comm_in : std_logic;
	signal comm_out : std_logic;

	signal output: std_logic_vector(7 downto 0);
	signal tx_strobe: std_logic := '0';
	signal tx_ready: std_logic;
	
	signal input: std_logic_vector(7 downto 0);
	signal rx_strobe: std_logic := '0';
	signal rx_ready: std_logic;
	
	signal send_counter: unsigned(7 downto 0) := to_unsigned(0, 8);
	signal receive_counter: unsigned(7 downto 0) := to_unsigned(0, 8);
	
	-- Clock period definitions
	constant clock_frequency: natural := 50000000;
	constant clock_period : time := 1000000000ns / clock_frequency;
	constant baud_rate: natural := 115200;
	constant baud_period: time := 1000000000ns / baud_rate;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
	uut: uart_loop
		generic map(clock_frequency, baud_rate, 8)
		PORT MAP (
			clock => clock,
			comm_out => comm_out,
			comm_in => comm_in
		);

	uut2: uart
		generic map(clock_frequency, baud_rate, 8)
		PORT MAP (
			clock,
			input, tx_strobe, tx_ready, comm_in,
			output, rx_strobe, rx_ready, comm_out
		);
	
	-- Clock process definitions
	clock_process :process
	begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
	end process;
 
	-- sending
	send_process: process
	begin
		input <= std_logic_vector(send_counter);
		send_counter <= send_counter + 1;
		wait for clock_period;

		if tx_ready = '0' then
			wait until tx_ready = '1';
		end if;
		
		tx_strobe <= '1';
		wait for clock_period * 1.5;
		tx_strobe <= '0';
	end process;

	-- receiving
	receive_process: process
	begin
		if rx_ready = '0' then
			wait until rx_ready = '1';
		end if;
		assert output = std_logic_vector(receive_counter);
		receive_counter <= receive_counter + 1;
		
		rx_strobe <= '1';
		wait for clock_period * 1.5;
		rx_strobe <= '0';
	end process;

END;
