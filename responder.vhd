

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity responder is
	port(
		reset: in std_logic;
		shift_in: in std_logic;
		data: in std_logic_vector(26 downto 0);
		matched: out std_logic;
		output: out std_logic_vector(16 downto 0) := (others => '0')
	);
end entity responder;

architecture arch of responder is
	constant zeroes: std_logic_vector(260 downto 0) := (others => '0');
	constant initial_states: std_logic_vector(260 downto 0) := "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001";
	constant final_states: std_logic_vector(260 downto 0) := "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100";

	signal states: std_logic_vector(260 downto 0) := "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001";
begin
	matched <= '0' when ((states and final_states) = zeroes) or (reset = '1') else '1';

	process(reset, shift_in)
	begin
		if reset = '1' then
			states <= initial_states;
		elsif shift_in'event and shift_in = '1' then
			states <= zeroes;

			if states(0) = '1' and std_match(data, "---------------------------") then
				states(0) <= '1';
			end if;

			if states(0) = '1' and std_match(data, "---------------------0-----") then
				states(1) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "---------------------100---") then
				states(3) <= '1';
			end if;

			if states(3) = '1' and std_match(data, "--------------------0------") then
				states(3) <= '1';
			end if;

			if states(3) = '1' and std_match(data, "--------------------1------") then
				states(4) <= '1';
				output(8 downto 0) <= "101000011";
			end if;

			if states(4) = '1' and std_match(data, "--------------------1------") then
				states(4) <= '1';
			end if;

			if states(4) = '1' and std_match(data, "--------------------0------") then
				states(5) <= '1';
				output(8) <= '0';
			end if;

			if states(5) = '1' and std_match(data, "--------------------0------") then
				states(5) <= '1';
			end if;

			if states(5) = '1' and std_match(data, "--------------------1------") then
				states(6) <= '1';
				output(8 downto 0) <= "101111010";
			end if;

			if states(6) = '1' and std_match(data, "--------------------1------") then
				states(6) <= '1';
			end if;

			if states(6) = '1' and std_match(data, "--------------------0------") then
				states(7) <= '1';
				output(8) <= '0';
			end if;

			if states(7) = '1' and std_match(data, "--------------------0------") then
				states(7) <= '1';
			end if;

			if states(7) = '1' and std_match(data, "--------------------1------") then
				states(8) <= '1';
				output(8 downto 0) <= "101100101";
			end if;

			if states(8) = '1' and std_match(data, "--------------------1------") then
				states(8) <= '1';
			end if;

			if states(8) = '1' and std_match(data, "--------------------0------") then
				states(9) <= '1';
				output(8) <= '0';
			end if;

			if states(9) = '1' and std_match(data, "--------------------0------") then
				states(9) <= '1';
			end if;

			if states(9) = '1' and std_match(data, "--------------------1------") then
				states(10) <= '1';
				output(8 downto 0) <= "101110011";
			end if;

			if states(10) = '1' and std_match(data, "--------------------1------") then
				states(10) <= '1';
			end if;

			if states(10) = '1' and std_match(data, "--------------------0------") then
				states(11) <= '1';
				output(8) <= '0';
			end if;

			if states(11) = '1' and std_match(data, "--------------------0------") then
				states(11) <= '1';
			end if;

			if states(11) = '1' and std_match(data, "--------------------1------") then
				states(12) <= '1';
				output(8 downto 0) <= "101100011";
			end if;

			if states(12) = '1' and std_match(data, "--------------------1------") then
				states(12) <= '1';
			end if;

			if states(12) = '1' and std_match(data, "--------------------0------") then
				states(13) <= '1';
				output(8) <= '0';
			end if;

			if states(13) = '1' and std_match(data, "--------------------0------") then
				states(13) <= '1';
			end if;

			if states(13) = '1' and std_match(data, "--------------------1------") then
				states(14) <= '1';
				output(8 downto 0) <= "100101100";
			end if;

			if states(14) = '1' and std_match(data, "--------------------1------") then
				states(14) <= '1';
			end if;

			if states(14) = '1' and std_match(data, "--------------------0------") then
				states(15) <= '1';
				output(8) <= '0';
			end if;

			if states(15) = '1' and std_match(data, "--------------------0------") then
				states(15) <= '1';
			end if;

			if states(15) = '1' and std_match(data, "--------------------1------") then
				states(16) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(16) = '1' and std_match(data, "--------------------1------") then
				states(16) <= '1';
			end if;

			if states(16) = '1' and std_match(data, "--------------------0------") then
				states(17) <= '1';
				output(8) <= '0';
			end if;

			if states(17) = '1' and std_match(data, "--------------------0------") then
				states(17) <= '1';
			end if;

			if states(17) = '1' and std_match(data, "--------------------1------") then
				states(18) <= '1';
				output(8 downto 0) <= "101110100";
			end if;

			if states(18) = '1' and std_match(data, "--------------------1------") then
				states(18) <= '1';
			end if;

			if states(18) = '1' and std_match(data, "--------------------0------") then
				states(19) <= '1';
				output(8) <= '0';
			end if;

			if states(19) = '1' and std_match(data, "--------------------0------") then
				states(19) <= '1';
			end if;

			if states(19) = '1' and std_match(data, "--------------------1------") then
				states(20) <= '1';
				output(8 downto 0) <= "101110101";
			end if;

			if states(20) = '1' and std_match(data, "--------------------1------") then
				states(20) <= '1';
			end if;

			if states(20) = '1' and std_match(data, "--------------------0------") then
				states(21) <= '1';
				output(8) <= '0';
			end if;

			if states(21) = '1' and std_match(data, "--------------------0------") then
				states(21) <= '1';
			end if;

			if states(21) = '1' and std_match(data, "--------------------1------") then
				states(22) <= '1';
				output(8 downto 0) <= "101110100";
			end if;

			if states(22) = '1' and std_match(data, "--------------------1------") then
				states(22) <= '1';
			end if;

			if states(22) = '1' and std_match(data, "--------------------0------") then
				states(23) <= '1';
				output(8) <= '0';
			end if;

			if states(23) = '1' and std_match(data, "--------------------0------") then
				states(23) <= '1';
			end if;

			if states(23) = '1' and std_match(data, "--------------------1------") then
				states(24) <= '1';
				output(8 downto 0) <= "101100001";
			end if;

			if states(24) = '1' and std_match(data, "--------------------1------") then
				states(24) <= '1';
			end if;

			if states(24) = '1' and std_match(data, "--------------------0------") then
				states(25) <= '1';
				output(8) <= '0';
			end if;

			if states(25) = '1' and std_match(data, "--------------------0------") then
				states(25) <= '1';
			end if;

			if states(25) = '1' and std_match(data, "--------------------1------") then
				states(26) <= '1';
				output(8 downto 0) <= "101101010";
			end if;

			if states(26) = '1' and std_match(data, "--------------------1------") then
				states(26) <= '1';
			end if;

			if states(26) = '1' and std_match(data, "--------------------0------") then
				states(27) <= '1';
				output(8) <= '0';
			end if;

			if states(27) = '1' and std_match(data, "--------------------0------") then
				states(27) <= '1';
			end if;

			if states(27) = '1' and std_match(data, "--------------------1------") then
				states(28) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(28) = '1' and std_match(data, "--------------------1------") then
				states(28) <= '1';
			end if;

			if states(28) = '1' and std_match(data, "--------------------0------") then
				states(29) <= '1';
				output(8) <= '0';
			end if;

			if states(29) = '1' and std_match(data, "--------------------0------") then
				states(29) <= '1';
			end if;

			if states(29) = '1' and std_match(data, "--------------------1------") then
				states(30) <= '1';
				output(8 downto 0) <= "101110101";
			end if;

			if states(30) = '1' and std_match(data, "--------------------1------") then
				states(30) <= '1';
			end if;

			if states(30) = '1' and std_match(data, "--------------------0------") then
				states(31) <= '1';
				output(8) <= '0';
			end if;

			if states(31) = '1' and std_match(data, "--------------------0------") then
				states(31) <= '1';
			end if;

			if states(31) = '1' and std_match(data, "--------------------1------") then
				states(32) <= '1';
				output(8 downto 0) <= "101101100";
			end if;

			if states(32) = '1' and std_match(data, "--------------------1------") then
				states(32) <= '1';
			end if;

			if states(32) = '1' and std_match(data, "--------------------0------") then
				states(33) <= '1';
				output(8) <= '0';
			end if;

			if states(33) = '1' and std_match(data, "--------------------0------") then
				states(33) <= '1';
			end if;

			if states(33) = '1' and std_match(data, "--------------------1------") then
				states(34) <= '1';
				output(8 downto 0) <= "101110100";
			end if;

			if states(34) = '1' and std_match(data, "--------------------1------") then
				states(34) <= '1';
			end if;

			if states(34) = '1' and std_match(data, "--------------------0------") then
				states(35) <= '1';
				output(8) <= '0';
			end if;

			if states(35) = '1' and std_match(data, "--------------------0------") then
				states(35) <= '1';
			end if;

			if states(35) = '1' and std_match(data, "--------------------1------") then
				states(36) <= '1';
				output(8 downto 0) <= "101110010";
			end if;

			if states(36) = '1' and std_match(data, "--------------------1------") then
				states(36) <= '1';
			end if;

			if states(36) = '1' and std_match(data, "--------------------0------") then
				states(37) <= '1';
				output(8) <= '0';
			end if;

			if states(37) = '1' and std_match(data, "--------------------0------") then
				states(37) <= '1';
			end if;

			if states(37) = '1' and std_match(data, "--------------------1------") then
				states(38) <= '1';
				output(8 downto 0) <= "101100001";
			end if;

			if states(38) = '1' and std_match(data, "--------------------1------") then
				states(38) <= '1';
			end if;

			if states(38) = '1' and std_match(data, "--------------------0------") then
				states(39) <= '1';
				output(8) <= '0';
			end if;

			if states(39) = '1' and std_match(data, "--------------------0------") then
				states(39) <= '1';
			end if;

			if states(39) = '1' and std_match(data, "--------------------1------") then
				states(40) <= '1';
				output(8 downto 0) <= "100101101";
			end if;

			if states(40) = '1' and std_match(data, "--------------------1------") then
				states(40) <= '1';
			end if;

			if states(40) = '1' and std_match(data, "--------------------0------") then
				states(41) <= '1';
				output(8) <= '0';
			end if;

			if states(41) = '1' and std_match(data, "--------------------0------") then
				states(41) <= '1';
			end if;

			if states(41) = '1' and std_match(data, "--------------------1------") then
				states(42) <= '1';
				output(8 downto 0) <= "101110100";
			end if;

			if states(42) = '1' and std_match(data, "--------------------1------") then
				states(42) <= '1';
			end if;

			if states(42) = '1' and std_match(data, "--------------------0------") then
				states(43) <= '1';
				output(8) <= '0';
			end if;

			if states(43) = '1' and std_match(data, "--------------------0------") then
				states(43) <= '1';
			end if;

			if states(43) = '1' and std_match(data, "--------------------1------") then
				states(44) <= '1';
				output(8 downto 0) <= "101110101";
			end if;

			if states(44) = '1' and std_match(data, "--------------------1------") then
				states(44) <= '1';
			end if;

			if states(44) = '1' and std_match(data, "--------------------0------") then
				states(45) <= '1';
				output(8) <= '0';
			end if;

			if states(45) = '1' and std_match(data, "--------------------0------") then
				states(45) <= '1';
			end if;

			if states(45) = '1' and std_match(data, "--------------------1------") then
				states(46) <= '1';
				output(8 downto 0) <= "101110010";
			end if;

			if states(46) = '1' and std_match(data, "--------------------1------") then
				states(46) <= '1';
			end if;

			if states(46) = '1' and std_match(data, "--------------------0------") then
				states(47) <= '1';
				output(8) <= '0';
			end if;

			if states(47) = '1' and std_match(data, "--------------------0------") then
				states(47) <= '1';
			end if;

			if states(47) = '1' and std_match(data, "--------------------1------") then
				states(48) <= '1';
				output(8 downto 0) <= "101100010";
			end if;

			if states(48) = '1' and std_match(data, "--------------------1------") then
				states(48) <= '1';
			end if;

			if states(48) = '1' and std_match(data, "--------------------0------") then
				states(49) <= '1';
				output(8) <= '0';
			end if;

			if states(49) = '1' and std_match(data, "--------------------0------") then
				states(49) <= '1';
			end if;

			if states(49) = '1' and std_match(data, "--------------------1------") then
				states(50) <= '1';
				output(8 downto 0) <= "101101111";
			end if;

			if states(50) = '1' and std_match(data, "--------------------1------") then
				states(50) <= '1';
			end if;

			if states(50) = '1' and std_match(data, "--------------------0------") then
				states(51) <= '1';
				output(8) <= '0';
			end if;

			if states(51) = '1' and std_match(data, "--------------------0------") then
				states(51) <= '1';
			end if;

			if states(51) = '1' and std_match(data, "--------------------1------") then
				states(52) <= '1';
				output(8 downto 0) <= "100101101";
			end if;

			if states(52) = '1' and std_match(data, "--------------------1------") then
				states(52) <= '1';
			end if;

			if states(52) = '1' and std_match(data, "--------------------0------") then
				states(53) <= '1';
				output(8) <= '0';
			end if;

			if states(53) = '1' and std_match(data, "--------------------0------") then
				states(53) <= '1';
			end if;

			if states(53) = '1' and std_match(data, "--------------------1------") then
				states(54) <= '1';
				output(8 downto 0) <= "101110101";
			end if;

			if states(54) = '1' and std_match(data, "--------------------1------") then
				states(54) <= '1';
			end if;

			if states(54) = '1' and std_match(data, "--------------------0------") then
				states(55) <= '1';
				output(8) <= '0';
			end if;

			if states(55) = '1' and std_match(data, "--------------------0------") then
				states(55) <= '1';
			end if;

			if states(55) = '1' and std_match(data, "--------------------1------") then
				states(56) <= '1';
				output(8 downto 0) <= "101101011";
			end if;

			if states(56) = '1' and std_match(data, "--------------------1------") then
				states(56) <= '1';
			end if;

			if states(56) = '1' and std_match(data, "--------------------0------") then
				states(57) <= '1';
				output(8) <= '0';
			end if;

			if states(57) = '1' and std_match(data, "--------------------0------") then
				states(57) <= '1';
			end if;

			if states(57) = '1' and std_match(data, "--------------------1------") then
				states(58) <= '1';
				output(8 downto 0) <= "101101100";
			end if;

			if states(58) = '1' and std_match(data, "--------------------1------") then
				states(58) <= '1';
			end if;

			if states(58) = '1' and std_match(data, "--------------------0------") then
				states(59) <= '1';
				output(8) <= '0';
			end if;

			if states(59) = '1' and std_match(data, "--------------------0------") then
				states(59) <= '1';
			end if;

			if states(59) = '1' and std_match(data, "--------------------1------") then
				states(60) <= '1';
				output(8 downto 0) <= "101100001";
			end if;

			if states(60) = '1' and std_match(data, "--------------------1------") then
				states(60) <= '1';
			end if;

			if states(60) = '1' and std_match(data, "--------------------0------") then
				states(61) <= '1';
				output(8) <= '0';
			end if;

			if states(61) = '1' and std_match(data, "--------------------0------") then
				states(61) <= '1';
			end if;

			if states(61) = '1' and std_match(data, "--------------------1------") then
				states(62) <= '1';
				output(8 downto 0) <= "101100100";
			end if;

			if states(62) = '1' and std_match(data, "--------------------1------") then
				states(62) <= '1';
			end if;

			if states(62) = '1' and std_match(data, "--------------------0------") then
				states(63) <= '1';
				output(8) <= '0';
			end if;

			if states(63) = '1' and std_match(data, "--------------------0------") then
				states(63) <= '1';
			end if;

			if states(63) = '1' and std_match(data, "--------------------1------") then
				states(64) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(64) = '1' and std_match(data, "--------------------1------") then
				states(64) <= '1';
			end if;

			if states(64) = '1' and std_match(data, "--------------------0------") then
				states(65) <= '1';
				output(8) <= '0';
			end if;

			if states(65) = '1' and std_match(data, "--------------------0------") then
				states(65) <= '1';
			end if;

			if states(65) = '1' and std_match(data, "--------------------1------") then
				states(66) <= '1';
				output(8 downto 0) <= "101111010";
			end if;

			if states(66) = '1' and std_match(data, "--------------------1------") then
				states(66) <= '1';
			end if;

			if states(66) = '1' and std_match(data, "--------------------0------") then
				states(67) <= '1';
				output(8) <= '0';
			end if;

			if states(67) = '1' and std_match(data, "--------------------0------") then
				states(67) <= '1';
			end if;

			if states(67) = '1' and std_match(data, "--------------------1------") then
				states(68) <= '1';
				output(8 downto 0) <= "101100010";
			end if;

			if states(68) = '1' and std_match(data, "--------------------1------") then
				states(68) <= '1';
			end if;

			if states(68) = '1' and std_match(data, "--------------------0------") then
				states(69) <= '1';
				output(8) <= '0';
			end if;

			if states(69) = '1' and std_match(data, "--------------------0------") then
				states(69) <= '1';
			end if;

			if states(69) = '1' and std_match(data, "--------------------1------") then
				states(70) <= '1';
				output(8 downto 0) <= "101110101";
			end if;

			if states(70) = '1' and std_match(data, "--------------------1------") then
				states(70) <= '1';
			end if;

			if states(70) = '1' and std_match(data, "--------------------0------") then
				states(71) <= '1';
				output(8) <= '0';
			end if;

			if states(71) = '1' and std_match(data, "--------------------0------") then
				states(71) <= '1';
			end if;

			if states(71) = '1' and std_match(data, "--------------------1------") then
				states(72) <= '1';
				output(8 downto 0) <= "101100100";
			end if;

			if states(72) = '1' and std_match(data, "--------------------1------") then
				states(72) <= '1';
			end if;

			if states(72) = '1' and std_match(data, "--------------------0------") then
				states(73) <= '1';
				output(8) <= '0';
			end if;

			if states(73) = '1' and std_match(data, "--------------------0------") then
				states(73) <= '1';
			end if;

			if states(73) = '1' and std_match(data, "--------------------1------") then
				states(74) <= '1';
				output(8 downto 0) <= "101101111";
			end if;

			if states(74) = '1' and std_match(data, "--------------------1------") then
				states(74) <= '1';
			end if;

			if states(74) = '1' and std_match(data, "--------------------0------") then
				states(75) <= '1';
				output(8) <= '0';
			end if;

			if states(75) = '1' and std_match(data, "--------------------0------") then
				states(75) <= '1';
			end if;

			if states(75) = '1' and std_match(data, "--------------------1------") then
				states(76) <= '1';
				output(8 downto 0) <= "101110111";
			end if;

			if states(76) = '1' and std_match(data, "--------------------1------") then
				states(76) <= '1';
			end if;

			if states(76) = '1' and std_match(data, "--------------------0------") then
				states(77) <= '1';
				output(8) <= '0';
			end if;

			if states(77) = '1' and std_match(data, "--------------------0------") then
				states(77) <= '1';
			end if;

			if states(77) = '1' and std_match(data, "--------------------1------") then
				states(78) <= '1';
				output(8 downto 0) <= "101100001";
			end if;

			if states(78) = '1' and std_match(data, "--------------------1------") then
				states(78) <= '1';
			end if;

			if states(78) = '1' and std_match(data, "--------------------0------") then
				states(79) <= '1';
				output(8) <= '0';
			end if;

			if states(79) = '1' and std_match(data, "--------------------0------") then
				states(79) <= '1';
			end if;

			if states(79) = '1' and std_match(data, "--------------------1------") then
				states(80) <= '1';
				output(8 downto 0) <= "101101110";
			end if;

			if states(80) = '1' and std_match(data, "--------------------1------") then
				states(80) <= '1';
			end if;

			if states(80) = '1' and std_match(data, "--------------------0------") then
				states(81) <= '1';
				output(8) <= '0';
			end if;

			if states(81) = '1' and std_match(data, "--------------------0------") then
				states(81) <= '1';
			end if;

			if states(81) = '1' and std_match(data, "--------------------1------") then
				states(82) <= '1';
				output(8 downto 0) <= "101111001";
			end if;

			if states(82) = '1' and std_match(data, "--------------------1------") then
				states(82) <= '1';
			end if;

			if states(82) = '1' and std_match(data, "--------------------0------") then
				states(83) <= '1';
				output(8) <= '0';
			end if;

			if states(83) = '1' and std_match(data, "--------------------0------") then
				states(83) <= '1';
			end if;

			if states(83) = '1' and std_match(data, "--------------------1------") then
				states(84) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(84) = '1' and std_match(data, "--------------------1------") then
				states(84) <= '1';
			end if;

			if states(84) = '1' and std_match(data, "--------------------0------") then
				states(85) <= '1';
				output(8) <= '0';
			end if;

			if states(85) = '1' and std_match(data, "--------------------0------") then
				states(85) <= '1';
			end if;

			if states(85) = '1' and std_match(data, "--------------------1------") then
				states(86) <= '1';
				output(8 downto 0) <= "101110000";
			end if;

			if states(86) = '1' and std_match(data, "--------------------1------") then
				states(86) <= '1';
			end if;

			if states(86) = '1' and std_match(data, "--------------------0------") then
				states(87) <= '1';
				output(8) <= '0';
			end if;

			if states(87) = '1' and std_match(data, "--------------------0------") then
				states(87) <= '1';
			end if;

			if states(87) = '1' and std_match(data, "--------------------1------") then
				states(88) <= '1';
				output(8 downto 0) <= "101110010";
			end if;

			if states(88) = '1' and std_match(data, "--------------------1------") then
				states(88) <= '1';
			end if;

			if states(88) = '1' and std_match(data, "--------------------0------") then
				states(89) <= '1';
				output(8) <= '0';
			end if;

			if states(89) = '1' and std_match(data, "--------------------0------") then
				states(89) <= '1';
			end if;

			if states(89) = '1' and std_match(data, "--------------------1------") then
				states(90) <= '1';
				output(8 downto 0) <= "101111010";
			end if;

			if states(90) = '1' and std_match(data, "--------------------1------") then
				states(90) <= '1';
			end if;

			if states(90) = '1' and std_match(data, "--------------------0------") then
				states(91) <= '1';
				output(8) <= '0';
			end if;

			if states(91) = '1' and std_match(data, "--------------------0------") then
				states(91) <= '1';
			end if;

			if states(91) = '1' and std_match(data, "--------------------1------") then
				states(92) <= '1';
				output(8 downto 0) <= "101100101";
			end if;

			if states(92) = '1' and std_match(data, "--------------------1------") then
				states(92) <= '1';
			end if;

			if states(92) = '1' and std_match(data, "--------------------0------") then
				states(93) <= '1';
				output(8) <= '0';
			end if;

			if states(93) = '1' and std_match(data, "--------------------0------") then
				states(93) <= '1';
			end if;

			if states(93) = '1' and std_match(data, "--------------------1------") then
				states(94) <= '1';
				output(8 downto 0) <= "101111010";
			end if;

			if states(94) = '1' and std_match(data, "--------------------1------") then
				states(94) <= '1';
			end if;

			if states(94) = '1' and std_match(data, "--------------------0------") then
				states(95) <= '1';
				output(8) <= '0';
			end if;

			if states(95) = '1' and std_match(data, "--------------------0------") then
				states(95) <= '1';
			end if;

			if states(95) = '1' and std_match(data, "--------------------1------") then
				states(96) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(96) = '1' and std_match(data, "--------------------1------") then
				states(96) <= '1';
			end if;

			if states(96) = '1' and std_match(data, "--------------------0------") then
				states(97) <= '1';
				output(8) <= '0';
			end if;

			if states(97) = '1' and std_match(data, "--------------------0------") then
				states(97) <= '1';
			end if;

			if states(97) = '1' and std_match(data, "--------------------1------") then
				states(98) <= '1';
				output(8 downto 0) <= "101001010";
			end if;

			if states(98) = '1' and std_match(data, "--------------------1------") then
				states(98) <= '1';
			end if;

			if states(98) = '1' and std_match(data, "--------------------0------") then
				states(99) <= '1';
				output(8) <= '0';
			end if;

			if states(99) = '1' and std_match(data, "--------------------0------") then
				states(99) <= '1';
			end if;

			if states(99) = '1' and std_match(data, "--------------------1------") then
				states(100) <= '1';
				output(8 downto 0) <= "101100001";
			end if;

			if states(100) = '1' and std_match(data, "--------------------1------") then
				states(100) <= '1';
			end if;

			if states(100) = '1' and std_match(data, "--------------------0------") then
				states(101) <= '1';
				output(8) <= '0';
			end if;

			if states(101) = '1' and std_match(data, "--------------------0------") then
				states(101) <= '1';
			end if;

			if states(101) = '1' and std_match(data, "--------------------1------") then
				states(102) <= '1';
				output(8 downto 0) <= "101101110";
			end if;

			if states(102) = '1' and std_match(data, "--------------------1------") then
				states(102) <= '1';
			end if;

			if states(102) = '1' and std_match(data, "--------------------0------") then
				states(103) <= '1';
				output(8) <= '0';
			end if;

			if states(103) = '1' and std_match(data, "--------------------0------") then
				states(103) <= '1';
			end if;

			if states(103) = '1' and std_match(data, "--------------------1------") then
				states(104) <= '1';
				output(8 downto 0) <= "101100001";
			end if;

			if states(104) = '1' and std_match(data, "--------------------1------") then
				states(104) <= '1';
			end if;

			if states(104) = '1' and std_match(data, "--------------------0------") then
				states(105) <= '1';
				output(8) <= '0';
			end if;

			if states(105) = '1' and std_match(data, "--------------------0------") then
				states(105) <= '1';
			end if;

			if states(105) = '1' and std_match(data, "--------------------1------") then
				states(106) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(106) = '1' and std_match(data, "--------------------1------") then
				states(106) <= '1';
			end if;

			if states(106) = '1' and std_match(data, "--------------------0------") then
				states(107) <= '1';
				output(8) <= '0';
			end if;

			if states(107) = '1' and std_match(data, "--------------------0------") then
				states(107) <= '1';
			end if;

			if states(107) = '1' and std_match(data, "--------------------1------") then
				states(108) <= '1';
				output(8 downto 0) <= "101010010";
			end if;

			if states(108) = '1' and std_match(data, "--------------------1------") then
				states(108) <= '1';
			end if;

			if states(108) = '1' and std_match(data, "--------------------0------") then
				states(109) <= '1';
				output(8) <= '0';
			end if;

			if states(109) = '1' and std_match(data, "--------------------0------") then
				states(109) <= '1';
			end if;

			if states(109) = '1' and std_match(data, "--------------------1------") then
				states(110) <= '1';
				output(8 downto 0) <= "101111001";
			end if;

			if states(110) = '1' and std_match(data, "--------------------1------") then
				states(110) <= '1';
			end if;

			if states(110) = '1' and std_match(data, "--------------------0------") then
				states(111) <= '1';
				output(8) <= '0';
			end if;

			if states(111) = '1' and std_match(data, "--------------------0------") then
				states(111) <= '1';
			end if;

			if states(111) = '1' and std_match(data, "--------------------1------") then
				states(112) <= '1';
				output(8 downto 0) <= "101100100";
			end if;

			if states(112) = '1' and std_match(data, "--------------------1------") then
				states(112) <= '1';
			end if;

			if states(112) = '1' and std_match(data, "--------------------0------") then
				states(113) <= '1';
				output(8) <= '0';
			end if;

			if states(113) = '1' and std_match(data, "--------------------0------") then
				states(113) <= '1';
			end if;

			if states(113) = '1' and std_match(data, "--------------------1------") then
				states(114) <= '1';
				output(8 downto 0) <= "101111010";
			end if;

			if states(114) = '1' and std_match(data, "--------------------1------") then
				states(114) <= '1';
			end if;

			if states(114) = '1' and std_match(data, "--------------------0------") then
				states(115) <= '1';
				output(8) <= '0';
			end if;

			if states(115) = '1' and std_match(data, "--------------------0------") then
				states(115) <= '1';
			end if;

			if states(115) = '1' and std_match(data, "--------------------1------") then
				states(116) <= '1';
				output(8 downto 0) <= "101100101";
			end if;

			if states(116) = '1' and std_match(data, "--------------------1------") then
				states(116) <= '1';
			end if;

			if states(116) = '1' and std_match(data, "--------------------0------") then
				states(117) <= '1';
				output(8) <= '0';
			end if;

			if states(117) = '1' and std_match(data, "--------------------0------") then
				states(117) <= '1';
			end if;

			if states(117) = '1' and std_match(data, "--------------------1------") then
				states(118) <= '1';
				output(8 downto 0) <= "101110111";
			end if;

			if states(118) = '1' and std_match(data, "--------------------1------") then
				states(118) <= '1';
			end if;

			if states(118) = '1' and std_match(data, "--------------------0------") then
				states(119) <= '1';
				output(8) <= '0';
			end if;

			if states(119) = '1' and std_match(data, "--------------------0------") then
				states(119) <= '1';
			end if;

			if states(119) = '1' and std_match(data, "--------------------1------") then
				states(120) <= '1';
				output(8 downto 0) <= "101110011";
			end if;

			if states(120) = '1' and std_match(data, "--------------------1------") then
				states(120) <= '1';
			end if;

			if states(120) = '1' and std_match(data, "--------------------0------") then
				states(121) <= '1';
				output(8) <= '0';
			end if;

			if states(121) = '1' and std_match(data, "--------------------0------") then
				states(121) <= '1';
			end if;

			if states(121) = '1' and std_match(data, "--------------------1------") then
				states(122) <= '1';
				output(8 downto 0) <= "101101011";
			end if;

			if states(122) = '1' and std_match(data, "--------------------1------") then
				states(122) <= '1';
			end if;

			if states(122) = '1' and std_match(data, "--------------------0------") then
				states(123) <= '1';
				output(8) <= '0';
			end if;

			if states(123) = '1' and std_match(data, "--------------------0------") then
				states(123) <= '1';
			end if;

			if states(123) = '1' and std_match(data, "--------------------1------") then
				states(124) <= '1';
				output(8 downto 0) <= "101101001";
			end if;

			if states(124) = '1' and std_match(data, "--------------------1------") then
				states(124) <= '1';
			end if;

			if states(124) = '1' and std_match(data, "--------------------0------") then
				states(125) <= '1';
				output(8) <= '0';
			end if;

			if states(125) = '1' and std_match(data, "--------------------0------") then
				states(125) <= '1';
			end if;

			if states(125) = '1' and std_match(data, "--------------------1------") then
				states(126) <= '1';
				output(8 downto 0) <= "101100101";
			end if;

			if states(126) = '1' and std_match(data, "--------------------1------") then
				states(126) <= '1';
			end if;

			if states(126) = '1' and std_match(data, "--------------------0------") then
				states(127) <= '1';
				output(8) <= '0';
			end if;

			if states(127) = '1' and std_match(data, "--------------------0------") then
				states(127) <= '1';
			end if;

			if states(127) = '1' and std_match(data, "--------------------1------") then
				states(128) <= '1';
				output(8 downto 0) <= "101100111";
			end if;

			if states(128) = '1' and std_match(data, "--------------------1------") then
				states(128) <= '1';
			end if;

			if states(128) = '1' and std_match(data, "--------------------0------") then
				states(129) <= '1';
				output(8) <= '0';
			end if;

			if states(129) = '1' and std_match(data, "--------------------0------") then
				states(129) <= '1';
			end if;

			if states(129) = '1' and std_match(data, "--------------------1------") then
				states(130) <= '1';
				output(8 downto 0) <= "101101111";
			end if;

			if states(130) = '1' and std_match(data, "--------------------1------") then
				states(130) <= '1';
			end if;

			if states(130) = '1' and std_match(data, "--------------------0------") then
				states(131) <= '1';
				output(8) <= '0';
			end if;

			if states(131) = '1' and std_match(data, "--------------------0------") then
				states(131) <= '1';
			end if;

			if states(131) = '1' and std_match(data, "--------------------1------") then
				states(132) <= '1';
				output(8 downto 0) <= "100101110";
			end if;

			if states(132) = '1' and std_match(data, "--------------------1------") then
				states(132) <= '1';
			end if;

			if states(132) = '1' and std_match(data, "--------------------0------") then
				states(133) <= '1';
				output(8) <= '0';
			end if;

			if states(133) = '1' and std_match(data, "--------------------0------") then
				states(133) <= '1';
			end if;

			if states(133) = '1' and std_match(data, "--------------------1------") then
				states(134) <= '1';
				output(8 downto 0) <= "100001101";
			end if;

			if states(134) = '1' and std_match(data, "--------------------1------") then
				states(134) <= '1';
			end if;

			if states(134) = '1' and std_match(data, "--------------------0------") then
				states(135) <= '1';
				output(8) <= '0';
			end if;

			if states(135) = '1' and std_match(data, "--------------------0------") then
				states(135) <= '1';
			end if;

			if states(135) = '1' and std_match(data, "--------------------1------") then
				states(136) <= '1';
				output(8 downto 0) <= "100001010";
			end if;

			if states(136) = '1' and std_match(data, "--------------------1------") then
				states(136) <= '1';
			end if;

			if states(136) = '1' and std_match(data, "--------------------0------") then
				states(2) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "---------------------111---") then
				states(137) <= '1';
			end if;

			if states(137) = '1' and std_match(data, "--------------------0------") then
				states(137) <= '1';
			end if;

			if states(137) = '1' and std_match(data, "--------------------1------") then
				states(138) <= '1';
				output(8 downto 0) <= "101100010";
			end if;

			if states(138) = '1' and std_match(data, "--------------------1------") then
				states(138) <= '1';
			end if;

			if states(138) = '1' and std_match(data, "--------------------0------") then
				states(139) <= '1';
				output(8) <= '0';
			end if;

			if states(139) = '1' and std_match(data, "--------------------0------") then
				states(139) <= '1';
			end if;

			if states(139) = '1' and std_match(data, "--------------------1------") then
				states(140) <= '1';
				output(8 downto 0) <= "101110100";
			end if;

			if states(140) = '1' and std_match(data, "--------------------1------") then
				states(140) <= '1';
			end if;

			if states(140) = '1' and std_match(data, "--------------------0------") then
				states(141) <= '1';
				output(8) <= '0';
			end if;

			if states(141) = '1' and std_match(data, "--------------------0------") then
				states(141) <= '1';
			end if;

			if states(141) = '1' and std_match(data, "--------------------1------") then
				states(142) <= '1';
				output(8 downto 0) <= "101101110";
			end if;

			if states(142) = '1' and std_match(data, "--------------------1------") then
				states(142) <= '1';
			end if;

			if states(142) = '1' and std_match(data, "--------------------0------") then
				states(143) <= '1';
				output(8) <= '0';
			end if;

			if states(143) = '1' and std_match(data, "--------------------0------") then
				states(143) <= '1';
			end if;

			if states(143) = '1' and std_match(data, "--------------------1------") then
				states(144) <= '1';
				output(8 downto 0) <= "100111010";
			end if;

			if states(144) = '1' and std_match(data, "--------------------1------") then
				states(144) <= '1';
			end if;

			if states(144) = '1' and std_match(data, "--------------------0------") then
				states(145) <= '1';
				output(8) <= '0';
			end if;

			if states(145) = '1' and std_match(data, "-------------------1-------") then
				states(147) <= '1';
			end if;

			if states(147) = '1' and std_match(data, "--------------------0------") then
				states(147) <= '1';
			end if;

			if states(147) = '1' and std_match(data, "--------------------1------") then
				states(148) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(148) = '1' and std_match(data, "--------------------1------") then
				states(148) <= '1';
			end if;

			if states(148) = '1' and std_match(data, "--------------------0------") then
				states(146) <= '1';
				output(8) <= '0';
			end if;

			if states(145) = '1' and std_match(data, "-------------------0-------") then
				states(149) <= '1';
			end if;

			if states(149) = '1' and std_match(data, "--------------------0------") then
				states(149) <= '1';
			end if;

			if states(149) = '1' and std_match(data, "--------------------1------") then
				states(150) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(150) = '1' and std_match(data, "--------------------1------") then
				states(150) <= '1';
			end if;

			if states(150) = '1' and std_match(data, "--------------------0------") then
				states(146) <= '1';
				output(8) <= '0';
			end if;

			if states(146) = '1' and std_match(data, "------------------1--------") then
				states(152) <= '1';
			end if;

			if states(152) = '1' and std_match(data, "--------------------0------") then
				states(152) <= '1';
			end if;

			if states(152) = '1' and std_match(data, "--------------------1------") then
				states(153) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(153) = '1' and std_match(data, "--------------------1------") then
				states(153) <= '1';
			end if;

			if states(153) = '1' and std_match(data, "--------------------0------") then
				states(151) <= '1';
				output(8) <= '0';
			end if;

			if states(146) = '1' and std_match(data, "------------------0--------") then
				states(154) <= '1';
			end if;

			if states(154) = '1' and std_match(data, "--------------------0------") then
				states(154) <= '1';
			end if;

			if states(154) = '1' and std_match(data, "--------------------1------") then
				states(155) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(155) = '1' and std_match(data, "--------------------1------") then
				states(155) <= '1';
			end if;

			if states(155) = '1' and std_match(data, "--------------------0------") then
				states(151) <= '1';
				output(8) <= '0';
			end if;

			if states(151) = '1' and std_match(data, "-----------------1---------") then
				states(157) <= '1';
			end if;

			if states(157) = '1' and std_match(data, "--------------------0------") then
				states(157) <= '1';
			end if;

			if states(157) = '1' and std_match(data, "--------------------1------") then
				states(158) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(158) = '1' and std_match(data, "--------------------1------") then
				states(158) <= '1';
			end if;

			if states(158) = '1' and std_match(data, "--------------------0------") then
				states(156) <= '1';
				output(8) <= '0';
			end if;

			if states(151) = '1' and std_match(data, "-----------------0---------") then
				states(159) <= '1';
			end if;

			if states(159) = '1' and std_match(data, "--------------------0------") then
				states(159) <= '1';
			end if;

			if states(159) = '1' and std_match(data, "--------------------1------") then
				states(160) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(160) = '1' and std_match(data, "--------------------1------") then
				states(160) <= '1';
			end if;

			if states(160) = '1' and std_match(data, "--------------------0------") then
				states(156) <= '1';
				output(8) <= '0';
			end if;

			if states(156) = '1' and std_match(data, "----------------1----------") then
				states(162) <= '1';
			end if;

			if states(162) = '1' and std_match(data, "--------------------0------") then
				states(162) <= '1';
			end if;

			if states(162) = '1' and std_match(data, "--------------------1------") then
				states(163) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(163) = '1' and std_match(data, "--------------------1------") then
				states(163) <= '1';
			end if;

			if states(163) = '1' and std_match(data, "--------------------0------") then
				states(161) <= '1';
				output(8) <= '0';
			end if;

			if states(156) = '1' and std_match(data, "----------------0----------") then
				states(164) <= '1';
			end if;

			if states(164) = '1' and std_match(data, "--------------------0------") then
				states(164) <= '1';
			end if;

			if states(164) = '1' and std_match(data, "--------------------1------") then
				states(165) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(165) = '1' and std_match(data, "--------------------1------") then
				states(165) <= '1';
			end if;

			if states(165) = '1' and std_match(data, "--------------------0------") then
				states(161) <= '1';
				output(8) <= '0';
			end if;

			if states(161) = '1' and std_match(data, "--------------------0------") then
				states(161) <= '1';
			end if;

			if states(161) = '1' and std_match(data, "--------------------1------") then
				states(166) <= '1';
				output(8 downto 0) <= "100100000";
			end if;

			if states(166) = '1' and std_match(data, "--------------------1------") then
				states(166) <= '1';
			end if;

			if states(166) = '1' and std_match(data, "--------------------0------") then
				states(167) <= '1';
				output(8) <= '0';
			end if;

			if states(167) = '1' and std_match(data, "--------------------0------") then
				states(167) <= '1';
			end if;

			if states(167) = '1' and std_match(data, "--------------------1------") then
				states(168) <= '1';
				output(8 downto 0) <= "101110011";
			end if;

			if states(168) = '1' and std_match(data, "--------------------1------") then
				states(168) <= '1';
			end if;

			if states(168) = '1' and std_match(data, "--------------------0------") then
				states(169) <= '1';
				output(8) <= '0';
			end if;

			if states(169) = '1' and std_match(data, "--------------------0------") then
				states(169) <= '1';
			end if;

			if states(169) = '1' and std_match(data, "--------------------1------") then
				states(170) <= '1';
				output(8 downto 0) <= "101110111";
			end if;

			if states(170) = '1' and std_match(data, "--------------------1------") then
				states(170) <= '1';
			end if;

			if states(170) = '1' and std_match(data, "--------------------0------") then
				states(171) <= '1';
				output(8) <= '0';
			end if;

			if states(171) = '1' and std_match(data, "--------------------0------") then
				states(171) <= '1';
			end if;

			if states(171) = '1' and std_match(data, "--------------------1------") then
				states(172) <= '1';
				output(8 downto 0) <= "100111010";
			end if;

			if states(172) = '1' and std_match(data, "--------------------1------") then
				states(172) <= '1';
			end if;

			if states(172) = '1' and std_match(data, "--------------------0------") then
				states(173) <= '1';
				output(8) <= '0';
			end if;

			if states(173) = '1' and std_match(data, "---------------1-----------") then
				states(175) <= '1';
			end if;

			if states(175) = '1' and std_match(data, "--------------------0------") then
				states(175) <= '1';
			end if;

			if states(175) = '1' and std_match(data, "--------------------1------") then
				states(176) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(176) = '1' and std_match(data, "--------------------1------") then
				states(176) <= '1';
			end if;

			if states(176) = '1' and std_match(data, "--------------------0------") then
				states(174) <= '1';
				output(8) <= '0';
			end if;

			if states(173) = '1' and std_match(data, "---------------0-----------") then
				states(177) <= '1';
			end if;

			if states(177) = '1' and std_match(data, "--------------------0------") then
				states(177) <= '1';
			end if;

			if states(177) = '1' and std_match(data, "--------------------1------") then
				states(178) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(178) = '1' and std_match(data, "--------------------1------") then
				states(178) <= '1';
			end if;

			if states(178) = '1' and std_match(data, "--------------------0------") then
				states(174) <= '1';
				output(8) <= '0';
			end if;

			if states(174) = '1' and std_match(data, "--------------1------------") then
				states(180) <= '1';
			end if;

			if states(180) = '1' and std_match(data, "--------------------0------") then
				states(180) <= '1';
			end if;

			if states(180) = '1' and std_match(data, "--------------------1------") then
				states(181) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(181) = '1' and std_match(data, "--------------------1------") then
				states(181) <= '1';
			end if;

			if states(181) = '1' and std_match(data, "--------------------0------") then
				states(179) <= '1';
				output(8) <= '0';
			end if;

			if states(174) = '1' and std_match(data, "--------------0------------") then
				states(182) <= '1';
			end if;

			if states(182) = '1' and std_match(data, "--------------------0------") then
				states(182) <= '1';
			end if;

			if states(182) = '1' and std_match(data, "--------------------1------") then
				states(183) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(183) = '1' and std_match(data, "--------------------1------") then
				states(183) <= '1';
			end if;

			if states(183) = '1' and std_match(data, "--------------------0------") then
				states(179) <= '1';
				output(8) <= '0';
			end if;

			if states(179) = '1' and std_match(data, "-------------1-------------") then
				states(185) <= '1';
			end if;

			if states(185) = '1' and std_match(data, "--------------------0------") then
				states(185) <= '1';
			end if;

			if states(185) = '1' and std_match(data, "--------------------1------") then
				states(186) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(186) = '1' and std_match(data, "--------------------1------") then
				states(186) <= '1';
			end if;

			if states(186) = '1' and std_match(data, "--------------------0------") then
				states(184) <= '1';
				output(8) <= '0';
			end if;

			if states(179) = '1' and std_match(data, "-------------0-------------") then
				states(187) <= '1';
			end if;

			if states(187) = '1' and std_match(data, "--------------------0------") then
				states(187) <= '1';
			end if;

			if states(187) = '1' and std_match(data, "--------------------1------") then
				states(188) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(188) = '1' and std_match(data, "--------------------1------") then
				states(188) <= '1';
			end if;

			if states(188) = '1' and std_match(data, "--------------------0------") then
				states(184) <= '1';
				output(8) <= '0';
			end if;

			if states(184) = '1' and std_match(data, "------------1--------------") then
				states(190) <= '1';
			end if;

			if states(190) = '1' and std_match(data, "--------------------0------") then
				states(190) <= '1';
			end if;

			if states(190) = '1' and std_match(data, "--------------------1------") then
				states(191) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(191) = '1' and std_match(data, "--------------------1------") then
				states(191) <= '1';
			end if;

			if states(191) = '1' and std_match(data, "--------------------0------") then
				states(189) <= '1';
				output(8) <= '0';
			end if;

			if states(184) = '1' and std_match(data, "------------0--------------") then
				states(192) <= '1';
			end if;

			if states(192) = '1' and std_match(data, "--------------------0------") then
				states(192) <= '1';
			end if;

			if states(192) = '1' and std_match(data, "--------------------1------") then
				states(193) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(193) = '1' and std_match(data, "--------------------1------") then
				states(193) <= '1';
			end if;

			if states(193) = '1' and std_match(data, "--------------------0------") then
				states(189) <= '1';
				output(8) <= '0';
			end if;

			if states(189) = '1' and std_match(data, "-----------1---------------") then
				states(195) <= '1';
			end if;

			if states(195) = '1' and std_match(data, "--------------------0------") then
				states(195) <= '1';
			end if;

			if states(195) = '1' and std_match(data, "--------------------1------") then
				states(196) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(196) = '1' and std_match(data, "--------------------1------") then
				states(196) <= '1';
			end if;

			if states(196) = '1' and std_match(data, "--------------------0------") then
				states(194) <= '1';
				output(8) <= '0';
			end if;

			if states(189) = '1' and std_match(data, "-----------0---------------") then
				states(197) <= '1';
			end if;

			if states(197) = '1' and std_match(data, "--------------------0------") then
				states(197) <= '1';
			end if;

			if states(197) = '1' and std_match(data, "--------------------1------") then
				states(198) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(198) = '1' and std_match(data, "--------------------1------") then
				states(198) <= '1';
			end if;

			if states(198) = '1' and std_match(data, "--------------------0------") then
				states(194) <= '1';
				output(8) <= '0';
			end if;

			if states(194) = '1' and std_match(data, "----------1----------------") then
				states(200) <= '1';
			end if;

			if states(200) = '1' and std_match(data, "--------------------0------") then
				states(200) <= '1';
			end if;

			if states(200) = '1' and std_match(data, "--------------------1------") then
				states(201) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(201) = '1' and std_match(data, "--------------------1------") then
				states(201) <= '1';
			end if;

			if states(201) = '1' and std_match(data, "--------------------0------") then
				states(199) <= '1';
				output(8) <= '0';
			end if;

			if states(194) = '1' and std_match(data, "----------0----------------") then
				states(202) <= '1';
			end if;

			if states(202) = '1' and std_match(data, "--------------------0------") then
				states(202) <= '1';
			end if;

			if states(202) = '1' and std_match(data, "--------------------1------") then
				states(203) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(203) = '1' and std_match(data, "--------------------1------") then
				states(203) <= '1';
			end if;

			if states(203) = '1' and std_match(data, "--------------------0------") then
				states(199) <= '1';
				output(8) <= '0';
			end if;

			if states(199) = '1' and std_match(data, "---------1-----------------") then
				states(205) <= '1';
			end if;

			if states(205) = '1' and std_match(data, "--------------------0------") then
				states(205) <= '1';
			end if;

			if states(205) = '1' and std_match(data, "--------------------1------") then
				states(206) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(206) = '1' and std_match(data, "--------------------1------") then
				states(206) <= '1';
			end if;

			if states(206) = '1' and std_match(data, "--------------------0------") then
				states(204) <= '1';
				output(8) <= '0';
			end if;

			if states(199) = '1' and std_match(data, "---------0-----------------") then
				states(207) <= '1';
			end if;

			if states(207) = '1' and std_match(data, "--------------------0------") then
				states(207) <= '1';
			end if;

			if states(207) = '1' and std_match(data, "--------------------1------") then
				states(208) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(208) = '1' and std_match(data, "--------------------1------") then
				states(208) <= '1';
			end if;

			if states(208) = '1' and std_match(data, "--------------------0------") then
				states(204) <= '1';
				output(8) <= '0';
			end if;

			if states(204) = '1' and std_match(data, "--------1------------------") then
				states(210) <= '1';
			end if;

			if states(210) = '1' and std_match(data, "--------------------0------") then
				states(210) <= '1';
			end if;

			if states(210) = '1' and std_match(data, "--------------------1------") then
				states(211) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(211) = '1' and std_match(data, "--------------------1------") then
				states(211) <= '1';
			end if;

			if states(211) = '1' and std_match(data, "--------------------0------") then
				states(209) <= '1';
				output(8) <= '0';
			end if;

			if states(204) = '1' and std_match(data, "--------0------------------") then
				states(212) <= '1';
			end if;

			if states(212) = '1' and std_match(data, "--------------------0------") then
				states(212) <= '1';
			end if;

			if states(212) = '1' and std_match(data, "--------------------1------") then
				states(213) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(213) = '1' and std_match(data, "--------------------1------") then
				states(213) <= '1';
			end if;

			if states(213) = '1' and std_match(data, "--------------------0------") then
				states(209) <= '1';
				output(8) <= '0';
			end if;

			if states(209) = '1' and std_match(data, "--------------------0------") then
				states(209) <= '1';
			end if;

			if states(209) = '1' and std_match(data, "--------------------1------") then
				states(214) <= '1';
				output(8 downto 0) <= "100001101";
			end if;

			if states(214) = '1' and std_match(data, "--------------------1------") then
				states(214) <= '1';
			end if;

			if states(214) = '1' and std_match(data, "--------------------0------") then
				states(215) <= '1';
				output(8) <= '0';
			end if;

			if states(215) = '1' and std_match(data, "--------------------0------") then
				states(215) <= '1';
			end if;

			if states(215) = '1' and std_match(data, "--------------------1------") then
				states(216) <= '1';
				output(8 downto 0) <= "100001010";
			end if;

			if states(216) = '1' and std_match(data, "--------------------1------") then
				states(216) <= '1';
			end if;

			if states(216) = '1' and std_match(data, "--------------------0------") then
				states(2) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-------1-------------110000") then
				states(218) <= '1';
			end if;

			if states(218) = '1' and std_match(data, "--------------------0------") then
				states(218) <= '1';
			end if;

			if states(218) = '1' and std_match(data, "--------------------1------") then
				states(219) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(219) = '1' and std_match(data, "--------------------1------") then
				states(219) <= '1';
			end if;

			if states(219) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-------0-------------110000") then
				states(220) <= '1';
			end if;

			if states(220) = '1' and std_match(data, "--------------------0------") then
				states(220) <= '1';
			end if;

			if states(220) = '1' and std_match(data, "--------------------1------") then
				states(221) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(221) = '1' and std_match(data, "--------------------1------") then
				states(221) <= '1';
			end if;

			if states(221) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "------1--------------110001") then
				states(222) <= '1';
			end if;

			if states(222) = '1' and std_match(data, "--------------------0------") then
				states(222) <= '1';
			end if;

			if states(222) = '1' and std_match(data, "--------------------1------") then
				states(223) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(223) = '1' and std_match(data, "--------------------1------") then
				states(223) <= '1';
			end if;

			if states(223) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "------0--------------110001") then
				states(224) <= '1';
			end if;

			if states(224) = '1' and std_match(data, "--------------------0------") then
				states(224) <= '1';
			end if;

			if states(224) = '1' and std_match(data, "--------------------1------") then
				states(225) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(225) = '1' and std_match(data, "--------------------1------") then
				states(225) <= '1';
			end if;

			if states(225) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-----1---------------110010") then
				states(226) <= '1';
			end if;

			if states(226) = '1' and std_match(data, "--------------------0------") then
				states(226) <= '1';
			end if;

			if states(226) = '1' and std_match(data, "--------------------1------") then
				states(227) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(227) = '1' and std_match(data, "--------------------1------") then
				states(227) <= '1';
			end if;

			if states(227) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-----0---------------110010") then
				states(228) <= '1';
			end if;

			if states(228) = '1' and std_match(data, "--------------------0------") then
				states(228) <= '1';
			end if;

			if states(228) = '1' and std_match(data, "--------------------1------") then
				states(229) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(229) = '1' and std_match(data, "--------------------1------") then
				states(229) <= '1';
			end if;

			if states(229) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "----1----------------110011") then
				states(230) <= '1';
			end if;

			if states(230) = '1' and std_match(data, "--------------------0------") then
				states(230) <= '1';
			end if;

			if states(230) = '1' and std_match(data, "--------------------1------") then
				states(231) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(231) = '1' and std_match(data, "--------------------1------") then
				states(231) <= '1';
			end if;

			if states(231) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "----0----------------110011") then
				states(232) <= '1';
			end if;

			if states(232) = '1' and std_match(data, "--------------------0------") then
				states(232) <= '1';
			end if;

			if states(232) = '1' and std_match(data, "--------------------1------") then
				states(233) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(233) = '1' and std_match(data, "--------------------1------") then
				states(233) <= '1';
			end if;

			if states(233) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "---1-----------------110100") then
				states(234) <= '1';
			end if;

			if states(234) = '1' and std_match(data, "--------------------0------") then
				states(234) <= '1';
			end if;

			if states(234) = '1' and std_match(data, "--------------------1------") then
				states(235) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(235) = '1' and std_match(data, "--------------------1------") then
				states(235) <= '1';
			end if;

			if states(235) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "---0-----------------110100") then
				states(236) <= '1';
			end if;

			if states(236) = '1' and std_match(data, "--------------------0------") then
				states(236) <= '1';
			end if;

			if states(236) = '1' and std_match(data, "--------------------1------") then
				states(237) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(237) = '1' and std_match(data, "--------------------1------") then
				states(237) <= '1';
			end if;

			if states(237) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "--1------------------110101") then
				states(238) <= '1';
			end if;

			if states(238) = '1' and std_match(data, "--------------------0------") then
				states(238) <= '1';
			end if;

			if states(238) = '1' and std_match(data, "--------------------1------") then
				states(239) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(239) = '1' and std_match(data, "--------------------1------") then
				states(239) <= '1';
			end if;

			if states(239) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "--0------------------110101") then
				states(240) <= '1';
			end if;

			if states(240) = '1' and std_match(data, "--------------------0------") then
				states(240) <= '1';
			end if;

			if states(240) = '1' and std_match(data, "--------------------1------") then
				states(241) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(241) = '1' and std_match(data, "--------------------1------") then
				states(241) <= '1';
			end if;

			if states(241) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-1-------------------110110") then
				states(242) <= '1';
			end if;

			if states(242) = '1' and std_match(data, "--------------------0------") then
				states(242) <= '1';
			end if;

			if states(242) = '1' and std_match(data, "--------------------1------") then
				states(243) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(243) = '1' and std_match(data, "--------------------1------") then
				states(243) <= '1';
			end if;

			if states(243) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-0-------------------110110") then
				states(244) <= '1';
			end if;

			if states(244) = '1' and std_match(data, "--------------------0------") then
				states(244) <= '1';
			end if;

			if states(244) = '1' and std_match(data, "--------------------1------") then
				states(245) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(245) = '1' and std_match(data, "--------------------1------") then
				states(245) <= '1';
			end if;

			if states(245) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "1--------------------110111") then
				states(246) <= '1';
			end if;

			if states(246) = '1' and std_match(data, "--------------------0------") then
				states(246) <= '1';
			end if;

			if states(246) = '1' and std_match(data, "--------------------1------") then
				states(247) <= '1';
				output(8 downto 0) <= "100110001";
			end if;

			if states(247) = '1' and std_match(data, "--------------------1------") then
				states(247) <= '1';
			end if;

			if states(247) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "0--------------------110111") then
				states(248) <= '1';
			end if;

			if states(248) = '1' and std_match(data, "--------------------0------") then
				states(248) <= '1';
			end if;

			if states(248) = '1' and std_match(data, "--------------------1------") then
				states(249) <= '1';
				output(8 downto 0) <= "100110000";
			end if;

			if states(249) = '1' and std_match(data, "--------------------1------") then
				states(249) <= '1';
			end if;

			if states(249) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
				output(8) <= '0';
			end if;

			if states(217) = '1' and std_match(data, "--------------------0------") then
				states(217) <= '1';
			end if;

			if states(217) = '1' and std_match(data, "--------------------1------") then
				states(250) <= '1';
				output(8 downto 0) <= "100001101";
			end if;

			if states(250) = '1' and std_match(data, "--------------------1------") then
				states(250) <= '1';
			end if;

			if states(250) = '1' and std_match(data, "--------------------0------") then
				states(251) <= '1';
				output(8) <= '0';
			end if;

			if states(251) = '1' and std_match(data, "--------------------0------") then
				states(251) <= '1';
			end if;

			if states(251) = '1' and std_match(data, "--------------------1------") then
				states(252) <= '1';
				output(8 downto 0) <= "100001010";
			end if;

			if states(252) = '1' and std_match(data, "--------------------1------") then
				states(252) <= '1';
			end if;

			if states(252) = '1' and std_match(data, "--------------------0------") then
				states(2) <= '1';
				output(8) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-------1-------------101000") then
				states(253) <= '1';
				output(9) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-------0-------------101000") then
				states(253) <= '1';
				output(9) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "------1--------------101001") then
				states(253) <= '1';
				output(10) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "------0--------------101001") then
				states(253) <= '1';
				output(10) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "-----1---------------101010") then
				states(253) <= '1';
				output(11) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-----0---------------101010") then
				states(253) <= '1';
				output(11) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "----1----------------101011") then
				states(253) <= '1';
				output(12) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "----0----------------101011") then
				states(253) <= '1';
				output(12) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "---1-----------------101100") then
				states(253) <= '1';
				output(13) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "---0-----------------101100") then
				states(253) <= '1';
				output(13) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "--1------------------101101") then
				states(253) <= '1';
				output(14) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "--0------------------101101") then
				states(253) <= '1';
				output(14) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "-1-------------------101110") then
				states(253) <= '1';
				output(15) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "-0-------------------101110") then
				states(253) <= '1';
				output(15) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "1--------------------101111") then
				states(253) <= '1';
				output(16) <= '0';
			end if;

			if states(1) = '1' and std_match(data, "0--------------------101111") then
				states(253) <= '1';
				output(16) <= '1';
			end if;

			if states(253) = '1' and std_match(data, "--------------------0------") then
				states(253) <= '1';
			end if;

			if states(253) = '1' and std_match(data, "--------------------1------") then
				states(254) <= '1';
				output(8 downto 0) <= "101001111";
			end if;

			if states(254) = '1' and std_match(data, "--------------------1------") then
				states(254) <= '1';
			end if;

			if states(254) = '1' and std_match(data, "--------------------0------") then
				states(255) <= '1';
				output(8) <= '0';
			end if;

			if states(255) = '1' and std_match(data, "--------------------0------") then
				states(255) <= '1';
			end if;

			if states(255) = '1' and std_match(data, "--------------------1------") then
				states(256) <= '1';
				output(8 downto 0) <= "101001011";
			end if;

			if states(256) = '1' and std_match(data, "--------------------1------") then
				states(256) <= '1';
			end if;

			if states(256) = '1' and std_match(data, "--------------------0------") then
				states(257) <= '1';
				output(8) <= '0';
			end if;

			if states(257) = '1' and std_match(data, "--------------------0------") then
				states(257) <= '1';
			end if;

			if states(257) = '1' and std_match(data, "--------------------1------") then
				states(258) <= '1';
				output(8 downto 0) <= "100001101";
			end if;

			if states(258) = '1' and std_match(data, "--------------------1------") then
				states(258) <= '1';
			end if;

			if states(258) = '1' and std_match(data, "--------------------0------") then
				states(259) <= '1';
				output(8) <= '0';
			end if;

			if states(259) = '1' and std_match(data, "--------------------0------") then
				states(259) <= '1';
			end if;

			if states(259) = '1' and std_match(data, "--------------------1------") then
				states(260) <= '1';
				output(8 downto 0) <= "100001010";
			end if;

			if states(260) = '1' and std_match(data, "--------------------1------") then
				states(260) <= '1';
			end if;

			if states(260) = '1' and std_match(data, "--------------------0------") then
				states(2) <= '1';
				output(8) <= '0';
			end if;

		end if;
	end process;
end architecture arch;

