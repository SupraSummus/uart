library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.utils.all;

entity frequency_divider is
	generic(n: natural);
	port(
		reset: in std_logic;
		clk_in: in std_logic;
		clk_out: out std_logic
	);
end entity frequency_divider;

architecture arch of frequency_divider is
	signal state: unsigned(binary_length(n) - 1 downto 0) := to_unsigned(0, binary_length(n));
	signal internal_clk_out: std_logic := '1';
begin
	clk_out <= internal_clk_out and not(reset);
	
	process(clk_in, reset) begin
		if reset = '1' then
			internal_clk_out <= '0';
			state <= to_unsigned(0, binary_length(n));
			
		elsif clk_in'event and clk_in = '1' then
			if state = to_unsigned(n - 1, binary_length(n)) then
				-- overflow - output still 0
				internal_clk_out <= '0';
				state <= to_unsigned(0, binary_length(n));
			
			elsif state > to_unsigned(n/2 - 1, binary_length(n)) then
				-- above half way up - output 0
				internal_clk_out <= '0';
				state <= state + 1;
			
			else
				-- below half way up
				internal_clk_out <= '1';
				state <= state + 1;
				
			end if;
		end if;
	end process;
end architecture arch;
