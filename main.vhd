library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity main is
	port(
		mclk: in std_logic;
		comm_in: in std_logic;
		comm_out: out std_logic;
		led: out std_logic_vector(7 downto 0);
		btn: in std_logic_vector(3 downto 0);
		sw: in std_logic_vector(7 downto 0)
	);
end entity main;

architecture arch of main is
	signal internal_comm_out: std_logic;
	signal internal_led: std_logic_vector (7 downto 0);
	
	signal rx_data: std_logic_vector (7 downto 0);
	signal rx_ready: std_logic;
	signal rx_strobe: std_logic;
	signal rx_matched: std_logic;
	signal rx_command: std_logic_vector(4 downto 0);
	
	signal tx_data: std_logic_vector (7 downto 0);
	signal tx_ready: std_logic;
	signal tx_strobe: std_logic;
	
	signal responder_in: std_logic_vector (26 downto 0);
	signal responder_out: std_logic_vector (16 downto 0);
	signal responder_tx_data: std_logic_vector (7 downto 0);
	signal responder_tx_strobe: std_logic;
	signal responder_tx_ready: std_logic;
begin
	led <= internal_led;
	
	uart_instance: entity uart
		generic map(50000000, 115200, 8)
		port map(
			'0', mclk,
			tx_data, tx_strobe, tx_ready, internal_comm_out,
			rx_data, rx_strobe, rx_ready, comm_in
		);
	rx_strobe <= '1';
	comm_out <= internal_comm_out;
	
	--
	-- command recognition
	--
	interface: entity matcher
		port map(
			'0', rx_ready, rx_data,
			rx_matched, rx_command
		);
	
	--
	-- responder
	--
	responder_in(4 downto 0) <= rx_command;
	responder_in(5) <= rx_matched;
	responder_in(6) <= responder_tx_ready;
	responder_in(10 downto 7) <= btn;
	responder_in(18 downto 11) <= sw;
	responder_in(26 downto 19) <= internal_led;
	
	responder_tx_data <= responder_out(7 downto 0);
	responder_tx_strobe <= responder_out(8);
	internal_led <= responder_out(16 downto 9);
	interface_back: entity responder
		port map(
			'0', mclk,
			responder_in,
			open, responder_out
		);
	
	--
	-- mixing outputs
	--
	tx_mixing: entity mixer
		port map(
			'0', mclk,
			rx_data, rx_ready, open,
			responder_tx_data, responder_tx_strobe, responder_tx_ready,
			tx_data, tx_strobe, tx_ready
		);
end architecture arch;