

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity matcher is
	port(
		reset: in std_logic;
		shift_in: in std_logic;
		data: in std_logic_vector(7 downto 0);
		matched: out std_logic;
		output: out std_logic_vector(4 downto 0) := (others => '0')
	);
end entity matcher;

architecture arch of matcher is
	constant zeroes: std_logic_vector(25 downto 0) := (others => '0');
	constant initial_states: std_logic_vector(25 downto 0) := "00000000000000000000000001";
	constant final_states: std_logic_vector(25 downto 0) := "10000000000000000000000000";

	signal states: std_logic_vector(25 downto 0) := "00000000000000000000000001";
begin
	matched <= '0' when ((states and final_states) = zeroes) or (reset = '1') else '1';

	process(reset, shift_in)
	begin
		if reset = '1' then
			states <= initial_states;
		elsif shift_in'event and shift_in = '1' then
			states <= zeroes;

			if states(0) = '1' and std_match(data, "--------") then
				states(0) <= '1';
			end if;

			if states(0) = '1' and std_match(data, "01101000") then
				states(2) <= '1';
			end if;

			if states(2) = '1' and std_match(data, "01100101") then
				states(3) <= '1';
			end if;

			if states(3) = '1' and std_match(data, "01101100") then
				states(4) <= '1';
			end if;

			if states(4) = '1' and std_match(data, "01101100") then
				states(5) <= '1';
			end if;

			if states(5) = '1' and std_match(data, "01101111") then
				states(1) <= '1';
				output(4 downto 3) <= "00";
			end if;

			if states(0) = '1' and std_match(data, "01101100") then
				states(6) <= '1';
			end if;

			if states(6) = '1' and std_match(data, "01100101") then
				states(7) <= '1';
			end if;

			if states(7) = '1' and std_match(data, "01100100") then
				states(8) <= '1';
			end if;

			if states(8) = '1' and std_match(data, "00100000") then
				states(9) <= '1';
			end if;

			if states(9) = '1' and std_match(data, "00110000") then
				states(10) <= '1';
				output(2 downto 0) <= "000";
			end if;

			if states(9) = '1' and std_match(data, "00110001") then
				states(10) <= '1';
				output(2 downto 0) <= "001";
			end if;

			if states(9) = '1' and std_match(data, "00110010") then
				states(10) <= '1';
				output(2 downto 0) <= "010";
			end if;

			if states(9) = '1' and std_match(data, "00110011") then
				states(10) <= '1';
				output(2 downto 0) <= "011";
			end if;

			if states(9) = '1' and std_match(data, "00110100") then
				states(10) <= '1';
				output(2 downto 0) <= "100";
			end if;

			if states(9) = '1' and std_match(data, "00110101") then
				states(10) <= '1';
				output(2 downto 0) <= "101";
			end if;

			if states(9) = '1' and std_match(data, "00110110") then
				states(10) <= '1';
				output(2 downto 0) <= "110";
			end if;

			if states(9) = '1' and std_match(data, "00110111") then
				states(10) <= '1';
				output(2 downto 0) <= "111";
			end if;

			if states(10) = '1' and std_match(data, "00100000") then
				states(11) <= '1';
			end if;

			if states(11) = '1' and std_match(data, "01110100") then
				states(12) <= '1';
			end if;

			if states(12) = '1' and std_match(data, "01101111") then
				states(13) <= '1';
			end if;

			if states(13) = '1' and std_match(data, "01100111") then
				states(14) <= '1';
			end if;

			if states(14) = '1' and std_match(data, "01100111") then
				states(15) <= '1';
			end if;

			if states(15) = '1' and std_match(data, "01101100") then
				states(16) <= '1';
			end if;

			if states(16) = '1' and std_match(data, "01100101") then
				states(1) <= '1';
				output(4 downto 3) <= "01";
			end if;

			if states(11) = '1' and std_match(data, "01100111") then
				states(17) <= '1';
			end if;

			if states(17) = '1' and std_match(data, "01100101") then
				states(18) <= '1';
			end if;

			if states(18) = '1' and std_match(data, "01110100") then
				states(1) <= '1';
				output(4 downto 3) <= "10";
			end if;

			if states(0) = '1' and std_match(data, "01100010") then
				states(19) <= '1';
			end if;

			if states(19) = '1' and std_match(data, "01110101") then
				states(20) <= '1';
			end if;

			if states(20) = '1' and std_match(data, "01110100") then
				states(21) <= '1';
			end if;

			if states(21) = '1' and std_match(data, "01110100") then
				states(22) <= '1';
			end if;

			if states(22) = '1' and std_match(data, "01101111") then
				states(23) <= '1';
			end if;

			if states(23) = '1' and std_match(data, "01101110") then
				states(24) <= '1';
			end if;

			if states(24) = '1' and std_match(data, "01110011") then
				states(1) <= '1';
				output(4 downto 3) <= "11";
			end if;

			if states(1) = '1' and std_match(data, "00001010") then
				states(25) <= '1';
			end if;

			if states(1) = '1' and std_match(data, "00001101") then
				states(25) <= '1';
			end if;

		end if;
	end process;
end architecture arch;

