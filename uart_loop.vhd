library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity uart_loop is
	generic(
		clock_frequency: natural;
		baud_rate: natural;
		nbit: natural := 8
	);
	port(
		reset: in std_logic;
		clock: in std_logic;
		comm_in: in std_logic;
		comm_out: out std_logic
	);
end entity uart_loop;

architecture arch of uart_loop is
	signal byte_buffer: std_logic_vector(nbit - 1 downto 0);
	signal buffer_full: std_logic := '0';
	
	signal output: std_logic_vector(nbit - 1 downto 0);
	signal tx_strobe: std_logic := '0';
	signal tx_ready: std_logic;
	
	signal input: std_logic_vector(nbit - 1 downto 0);
	signal rx_strobe: std_logic := '0';
	signal rx_ready: std_logic;
begin
	comm: entity work.uart
		generic map(clock_frequency, baud_rate, nbit)
		port map(
			reset, clock,
			output, tx_strobe, tx_ready, comm_out,
			input, rx_strobe, rx_ready, comm_in
		);

	process(reset, clock) begin
		if reset = '1' then
			buffer_full <= '0';
			tx_strobe <= '0';
			rx_strobe <= '0';
			
		elsif clock'event and clock = '1' then
			-- handle rx
			if rx_ready = '1' then
				if rx_strobe = '1' then
					-- do nothing, waiting for rx module
				elsif buffer_full = '0' then
					byte_buffer <= input;
					buffer_full <= '1';
					rx_strobe <= '1';
				else
					-- oops, too fast
					rx_strobe <= '1';
				end if;
				
			else
				rx_strobe <= '0';
			end if;
			
			-- handle tx
			if tx_ready = '1' and buffer_full = '1' and tx_strobe = '0' then
				output <= byte_buffer;
				buffer_full <= '0';
				tx_strobe <= '1';
			
			elsif tx_ready = '0' then
				tx_strobe <= '0';
			end if;
		end if;
	end process;
end architecture arch;
