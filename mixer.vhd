library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity mixer is
	generic(
		nbit: natural := 8
	);
	port(
		reset: in std_logic;
		clock: in std_logic;
		
		data0: in std_logic_vector(nbit - 1 downto 0);
		strobe0: in std_logic;
		ready0: out std_logic := '1';
		
		data1: in std_logic_vector(nbit - 1 downto 0);
		strobe1: in std_logic;
		ready1: out std_logic := '1';
		
		data: out std_logic_vector(nbit - 1 downto 0);
		out_strobe: out std_logic := '0';
		ready: in std_logic
	);
end entity mixer;

architecture arch of mixer is
	signal buffer0: std_logic_vector (nbit - 1 downto 0);
	signal buffer0_used: std_logic := '0';
	
	signal buffer1: std_logic_vector (nbit - 1 downto 0);
	signal buffer1_used: std_logic := '0';
	
	signal strobe: std_logic := '0';
begin
	out_strobe <= strobe;

	ready0 <= '1' when buffer0_used = '0' else '0';
	ready1 <= '1' when buffer1_used = '0' else '0';

	process(clock, reset) begin
		if reset = '1' then
			buffer0_used <= '0';
			buffer1_used <= '0';
			strobe <= '0';
			
		elsif clock'event and clock = '1' then
			
			if buffer0_used = '0' and strobe0 = '1' then
				buffer0 <= data0;
				buffer0_used <= '1';
			end if;
			
			if buffer1_used = '0' and strobe1 = '1' then
				buffer1 <= data1;
				buffer1_used <= '1';
			end if;
			
			if ready = '1' and strobe = '0' then
				if buffer0_used = '1' then
					data <= buffer0;
					strobe <= '1';
					buffer0_used <= '0';
				elsif buffer1_used = '1' then
					data <= buffer1;
					strobe <= '1';
					buffer1_used <= '0';
				end if;
			end if;
			
			if ready = '0' then
				strobe <= '0';
			end if;
			
		end if;
	end process;
end architecture arch;