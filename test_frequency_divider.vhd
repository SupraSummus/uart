--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:   13:05:00 12/02/2014
-- Design Name:
-- Module Name:   D:/win/Xilinx/workspace/zadanie2/test_frequency_divider.vhd
-- Project Name:  zadanie2
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: frequency_divider
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

ENTITY test_frequency_divider IS
END test_frequency_divider;

ARCHITECTURE behavior OF test_frequency_divider IS
	component frequency_divider
	generic(n: natural);
	port(
		reset: in std_logic;
		clk_in: in std_logic;
		clk_out: out std_logic
	);
	end component;

   --Inputs
   signal clk_in : std_logic := '0';
   signal reset: std_logic;

	--Outputs
   signal clk_out : std_logic;

	constant clk_period: time := 10ns;
	constant divide_by: natural := 5;
	constant init_time: time := 20ns;
BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: frequency_divider
		generic map(divide_by)
		PORT MAP(reset, clk_in, clk_out);
	
	-- Clock process definitions
	clk_in_process :process
	begin
		clk_in <= '0';
		wait for clk_period/2;
		clk_in <= '1';
		wait for clk_period/2;
	end process;
	
	-- Stimulus process
	stim_proc: process
	begin
		reset <= '1';
		wait for clk_period * 3;
		assert clk_out = '0';
		reset <= '0';
		
		wait for clk_period*divide_by/4;
		assert(clk_out = '1');
		wait for clk_period*divide_by/2;
		assert(clk_out = '0');
		wait for clk_period*divide_by/4;
		
		reset <= '1';
		wait for clk_period * 7;
		reset <= '0';
		
		wait for clk_period*divide_by/4;
		assert(clk_out = '1');
		wait for clk_period*divide_by/2;
		assert(clk_out = '0');
		wait for clk_period*divide_by/4;
		
		wait for clk_period*divide_by*10;
		
		wait for clk_period*divide_by/4;
		assert(clk_out = '1');
		wait for clk_period*divide_by/2;
		assert(clk_out = '0');
		wait for clk_period*divide_by/4;
		
		wait for clk_period*divide_by/4;
		assert(clk_out = '1');
		wait for clk_period*divide_by/4 + 1ns;
		reset <= '1';
		wait for clk_period;
		assert(clk_out = '0');
		
		wait;
	end process;
	
END;