library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.utils.all;

entity universal_asynchronous_transmitter is
	generic(
		clock_frequency: natural;
		baud_rate: natural;
		nbit: natural := 8
	);
	port(
		reset: in std_logic;
		clock: in std_logic;
		input: in std_logic_vector(nbit - 1 downto 0);
		strobe: in std_logic;
		ready: out std_logic := '1';
		output: out std_logic := '1'
	);
end entity universal_asynchronous_transmitter;

architecture arch of universal_asynchronous_transmitter is
	type STATE_TYPE is (
		idle,
		sending
	);
	signal state: STATE_TYPE := idle;
	
	signal internal_input: std_logic_vector(nbit + 1 downto 0);
	signal bit_clock: std_logic;
	signal last_bit_clock: std_logic;
	signal bit_clock_divider_reset: std_logic := '1';
	signal bit_pointer: unsigned(binary_length(nbit + 2) - 1 downto 0);
begin
	bit_clock_divider: entity work.frequency_divider
		generic map(clock_frequency/baud_rate)
		port map(bit_clock_divider_reset, clock, bit_clock);
	
	internal_input(0) <= '0'; -- start bit
	internal_input(nbit + 1) <= '1'; -- stop bit
	
	process(clock) begin
		if clock'event and clock = '1' then
			if reset = '1' then
				state <= idle;
				output <= '1';
				bit_clock_divider_reset <= '1';
				ready <= '1';
				
			elsif state = idle and strobe = '1' then -- begin transmission
				-- lock input, not ready, bit pointer = 0
				internal_input(nbit downto 1) <= input;
				ready <= '0';
				bit_pointer <= to_unsigned(0, binary_length(nbit + 2));
				
				-- start bit clock
				bit_clock_divider_reset <= '0';
				last_bit_clock <= '0';
				
				state <= sending;
				
			elsif state = sending then
				if bit_clock = '1' and last_bit_clock = '0' then -- it 's time to transmit next bit
					last_bit_clock <= bit_clock;
					
					if bit_pointer <= nbit + 1 then
						-- send bit
						output <= internal_input(to_integer(bit_pointer));
						bit_pointer <= bit_pointer + 1;
					else
						-- we reached end of byte, disable bit clock
						bit_clock_divider_reset <= '1';
						
						-- ready to send next byte
						state <= idle;
						ready <= '1';
					end if;
					
				elsif bit_clock = '0' and last_bit_clock = '1' then
					last_bit_clock <= bit_clock;
					
				end if;
				
			end if;
		end if;
	end process;
end architecture arch;