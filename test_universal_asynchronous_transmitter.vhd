--------------------------------------------------------------------------------
-- Company: Keep It Simple Labs
-- Engineer: Jan Rydzewski
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_universal_asynchronous_transmitter IS
END test_universal_asynchronous_transmitter;
 
ARCHITECTURE behavior OF test_universal_asynchronous_transmitter IS 
 
	-- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT universal_asynchronous_transmitter
	generic(
		clock_frequency: natural;
		baud_rate: natural;
		nbit: natural := 8
	);
	PORT(
		reset: in std_logic;
		 clock : IN  std_logic;
		 input : IN  std_logic_vector(nbit - 1 downto 0);
		 strobe : IN  std_logic;
		 ready : OUT  std_logic;
		 output : OUT  std_logic
		);
	END COMPONENT;
	

   --Inputs
   signal clock : std_logic := '0';
   signal input : std_logic_vector(5 downto 0) := (others => '0');
   signal strobe : std_logic := '0';

 	--Outputs
   signal ready : std_logic;
   signal output : std_logic;

   -- Clock period definitions
   constant clock_frequency: natural := 50000000;
   constant clock_period : time := 1000000000ns / clock_frequency;
   constant baud_rate: natural := 115200;
   constant baud_period: time := 1000000000ns / baud_rate;
 
BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: universal_asynchronous_transmitter
	generic map(clock_frequency, baud_rate, 6)
	PORT MAP (
		'0',
		clock => clock,
		input => input,
		strobe => strobe,
		ready => ready,
		output => output
	);

	-- Clock process definitions
	clock_process :process
	begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
	end process;
 

	-- Stimulus process
	stim_proc: process
	begin
		wait for 50ns;
	
		input <= "101010";
		if ready = '0' then
			wait until ready = '1';
		end if;
		assert output = '1';
		
		strobe <= '1';
		if output = '1' then
			wait until output = '0';
		end if;
		strobe <= '0';
		assert ready = '0';
		
		wait for baud_period/2;
		assert output = '0';
		wait for baud_period;
		assert output = '0';
		wait for baud_period;
		assert output = '1';
		wait for baud_period;
		assert output = '0';
		wait for baud_period;
		assert output = '1';
		wait for baud_period;
		assert output = '0';
		wait for baud_period;
		assert output = '1';
		wait for baud_period;
		assert output = '1';
		assert ready = '0';
		wait for baud_period;
		assert ready = '1';
		
		strobe <= '1';
		input <= "111111";
		wait for baud_period*7.5;
		assert output = '1';
		wait for baud_period*8;
		assert output = '1';
		wait for baud_period*0.5;
		
		wait;
	end process;

END;
