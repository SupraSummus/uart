library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.utils.all;

entity universal_asynchronous_receiver is
	generic(
		clock_frequency: natural;
		baud_rate: natural;
		nbit: natural := 8;
		samples: natural := 9
	);
	port(
		reset: in std_logic;
		clock: in std_logic;
		output: out std_logic_vector(nbit - 1 downto 0) := (others => '0');
		strobe: in std_logic;
		ready: out std_logic := '0';
		input: in std_logic
	);
end entity universal_asynchronous_receiver;

architecture arch of universal_asynchronous_receiver is
	type STATE_TYPE is (
		waiting_for_transmission,
		delaying4,
		receiving,
		received_byte
	);
	signal state: STATE_TYPE := waiting_for_transmission;
	
	signal bit_pointer: unsigned(binary_length(nbit + 2) - 1 downto 0);
	signal baud_clock: std_logic;
	signal last_baud_clock: std_logic;
	signal baud_clock_reset: std_logic;
	
	signal sample_clock: std_logic;
	signal last_sample_clock: std_logic;
	
	signal delay4: std_logic;
	signal last_delay4: std_logic;
	signal delay4_reset: std_logic;
begin
	-- clocks
	delay4_reset <= '0' when state = delaying4 else '1';
	delay4_divider: entity work.frequency_divider
		generic map(clock_frequency/baud_rate/2)
		port map(delay4_reset, clock, delay4);
	
	baud_clock_reset <= '0' when state = receiving else '1';
	baud_clock_divider: entity work.frequency_divider
		generic map(clock_frequency/baud_rate)
		port map(baud_clock_reset, clock, baud_clock);
	
	sample_clock_divider: entity work.frequency_divider
		generic map(clock_frequency/baud_rate/samples/2)
		port map(not(baud_clock), clock, sample_clock);
	
	-- static
	ready <= '1' when state = received_byte else '0';
	
	process(clock, reset)
		variable ones: unsigned(binary_length(samples) - 1 downto 0);
		variable zeroes: unsigned(binary_length(samples) - 1 downto 0);
		variable average_input: std_logic;
	begin
		if reset = '1' then
			state <= waiting_for_transmission;
			
		elsif clock'event and clock = '1' then
			last_baud_clock <= baud_clock;
			last_sample_clock <= sample_clock;
			last_delay4 <= delay4;
		
			if state = waiting_for_transmission then
				if  input = '0' then -- detected start of transmission
					state <= delaying4;
				end if;
				
			elsif state = delaying4 then
				if delay4 = '0' and last_delay4 = '1' then -- 1/4 baud period passed
					state <= receiving;
					bit_pointer <= to_unsigned(0, binary_length(nbit + 2));
				end if;
				
			elsif state = receiving then
				if baud_clock = '1' and last_baud_clock = '0' then -- baud_clock rising edge
					zeroes := to_unsigned(0, binary_length(samples));
					ones := to_unsigned(0, binary_length(samples));
					
				elsif baud_clock = '0' and last_baud_clock = '1' then -- baud_clock falling edge
					if zeroes > ones then
						average_input := '0';
					else
						average_input := '1';
					end if;
					
					if bit_pointer = 0 then
						-- receive start bit
						if average_input = '0' then
							bit_pointer <= bit_pointer + 1;
						else
							-- expected 0 - start bit
							state <= waiting_for_transmission;
						end if;
					
					elsif bit_pointer < to_unsigned(nbit + 1, binary_length(nbit + 2)) then
						-- receive bit
						output(to_integer(bit_pointer - 1)) <= average_input;
						bit_pointer <= bit_pointer + 1;
						
					else
						-- receive stop
						if average_input = '1' then
							-- end of transmission
							state <= received_byte;
						else
							-- expected 1 stop bit
							state <= waiting_for_transmission;
						end if;
					end if;
					
				end if;
				
				if sample_clock = '1' and last_sample_clock = '0' then -- rising edge
					if input = '0' then
						zeroes := zeroes + 1;
					else
						ones := ones + 1;
					end if;
				end if;
				
			elsif state = received_byte and strobe = '1'  then -- prepare for next byte
				state <= waiting_for_transmission;
				
			end if;
		end if;
	end process;
end architecture arch;