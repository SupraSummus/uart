/**
 * helpers
 */

/**
input is
	(4 downto 0) - command
	5 - command matched
	6 - tx ready
	(10 downto 7) - pushbuttons
	(18 downto 11) - switches
	(26 downto 19) - leds

output is
	(7 downto 0) - tx data
	8 - tx strobe
	(16 downto 9) - leds
*/

var input = function (args) {
	if (args === undefined) args = {};
	
	if (args.command === undefined) args.command = '';
	if (args.buttons === undefined) args.buttons = '';
	if (args.switches === undefined) args.switches = '';
	if (args.leds === undefined) args.leds = '';
	
	args.command = args.command.split('').reverse();
	args.buttons = args.buttons.split('').reverse();
	args.switches = args.switches.split('').reverse();
	args.leds = args.leds.split('').reverse();
	
	return [
		args.command[0] || '-',
		args.command[1] || '-',
		args.command[2] || '-',
		args.command[3] || '-',
		args.command[4] || '-',
		
		args.commandMatched || '-',
		
		args.txReady  || '-',
		
		args.buttons[0] || '-',
		args.buttons[1] || '-',
		args.buttons[2] || '-',
		args.buttons[3] || '-',
		
		args.switches[0] || '-',
		args.switches[1] || '-',
		args.switches[2] || '-',
		args.switches[3] || '-',
		args.switches[4] || '-',
		args.switches[5] || '-',
		args.switches[6] || '-',
		args.switches[7] || '-',
		
		args.leds[0] || '-',
		args.leds[1] || '-',
		args.leds[2] || '-',
		args.leds[3] || '-',
		args.leds[4] || '-',
		args.leds[5] || '-',
		args.leds[6] || '-',
		args.leds[7] || '-',
	].reverse().join('');
};

var leafWithCode = function (label, code) {
	var leaf = Regexp.leaf(label);
	leaf.code = code;
	return leaf;
};

var toBinary = function (number, length) {
	var binary = number.toString(2);
	var padding = Array(length - binary.length + 1).join('0');
	return '' + padding + binary;
};

var regexpSendString = function (string) {
	var regexp = Regexp.and(string.split('').map(function (ch) {
		return Regexp.and([
			Regexp.many(Regexp.leaf(input({txReady: '0'}))), // wait if tx not ready
			leafWithCode(input({txReady: '1'}), 'output(8 downto 0) <= "1' + toBinary(ch.charCodeAt(0), 8) + '";'),
			Regexp.many(Regexp.leaf(input({txReady: '1'}))), // wait if tx still ready
			leafWithCode(input({txReady: '0'}), 'output(8) <= \'0\';'),
		]);
	}));
	return regexp;
};

/**
 * regexp definition
 */
var regexp = Regexp.and([
	Regexp.many(Regexp.leaf(input({}))),
	Regexp.leaf(input({commandMatched: '0'})),
	Regexp.or([
		
		// hello
		Regexp.and([
			Regexp.leaf(input({commandMatched: '1', command: '00---'})),
			regexpSendString(
				'Czesc, tutaj ultra-turbo-uklad zbudowany przez Jana Rydzewskiego.\r\n'// +
				//'Odpowiadam pelnym zdaniem (co prawda bez polskich znakow). Nie oznacza to wcale, iz jestem automatem stanowym.\r\n'
			),
		]),
		
		// buttons
		Regexp.and([
			Regexp.leaf(input({commandMatched: '1', command: '11---'})),
			regexpSendString('btn:'),
			Regexp.and(
				[0, 1, 2, 3].map(function (i) {
					var buttonMatcher = ['-', '-', '-', '-'];
					buttonMatcher[i] = '1';
					var positiveMatcher = buttonMatcher.reverse().join('');
					
					buttonMatcher = ['-', '-', '-', '-'];
					buttonMatcher[i] = '0';
					var negativeMatcher = buttonMatcher.reverse().join('');
					
					return Regexp.or([
						Regexp.and([
							Regexp.leaf(input({buttons: positiveMatcher})),
							regexpSendString('1'),
						]),
						Regexp.and([
							Regexp.leaf(input({buttons: negativeMatcher})),
							regexpSendString('0'),
						]),
					]);
				})
			),
			regexpSendString(' sw:'),
			Regexp.and(
				[0, 1, 2, 3, 4, 5, 6, 7].map(function (i) {
					var buttonMatcher = ['-', '-', '-', '-', '-', '-', '-', '-'];
					buttonMatcher[i] = '1';
					var positiveMatcher = buttonMatcher.reverse().join('');
					
					buttonMatcher = ['-', '-', '-', '-', '-', '-', '-', '-'];
					buttonMatcher[i] = '0';
					var negativeMatcher = buttonMatcher.reverse().join('');
					
					return Regexp.or([
						Regexp.and([
							Regexp.leaf(input({switches: positiveMatcher})),
							regexpSendString('1'),
						]),
						Regexp.and([
							Regexp.leaf(input({switches: negativeMatcher})),
							regexpSendString('0'),
						]),
					]);
				})
			),
			regexpSendString('\r\n'),
		]),
		
		// led get
		Regexp.and([
			Regexp.or(
				[0, 1, 2, 3, 4, 5, 6, 7].map(function (i) {
					var matcher = ['-', '-', '-', '-', '-', '-', '-', '-'];
					matcher[i] = '1';
					var positiveMatcher = matcher.reverse().join('');
					
					matcher = ['-', '-', '-', '-', '-', '-', '-', '-'];
					matcher[i] = '0';
					var negativeMatcher = matcher.reverse().join('');
					
					return Regexp.or([
						Regexp.and([
							Regexp.leaf(input({
								leds: positiveMatcher,
								commandMatched: '1',
								command: '10' + toBinary(i, 3),
							})),
							regexpSendString('1'),
						]),
						Regexp.and([
							Regexp.leaf(input({
								leds: negativeMatcher,
								commandMatched: '1',
								command: '10' + toBinary(i, 3),
							})),
							regexpSendString('0'),
						]),
					]);
				})
			),
			regexpSendString('\r\n'),
		]),
		
		// led toggle
		Regexp.and([
			Regexp.or(
				[0, 1, 2, 3, 4, 5, 6, 7].map(function (i) {
					var matcher = ['-', '-', '-', '-', '-', '-', '-', '-'];
					matcher[i] = '1';
					var positiveMatcher = matcher.reverse().join('');
					
					matcher = ['-', '-', '-', '-', '-', '-', '-', '-'];
					matcher[i] = '0';
					var negativeMatcher = matcher.reverse().join('');
					
					return Regexp.or([
						leafWithCode(input({
							leds: positiveMatcher,
							commandMatched: '1',
							command: '01' + toBinary(i, 3),
						}), 'output(' + (i + 9) + ') <= \'0\';'),
						leafWithCode(input({
							leds: negativeMatcher,
							commandMatched: '1',
							command: '01' + toBinary(i, 3),
						}), 'output(' + (i + 9) + ') <= \'1\';'),
					]);

				})
			),
			regexpSendString('OK\r\n'),
		]),
	]),
]);
console.log('regexp', regexp);

/**
 * automaton
 */

var automaton = Automaton.fromRegexp(regexp);
console.log('automaton', automaton);

/**
 * vhdl code
 */
var code = VHDLMatcher.generate(automaton, input({}).length, 17, 'responder');
console.log('code', code);
document.getElementById('code').innerHTML = code;
