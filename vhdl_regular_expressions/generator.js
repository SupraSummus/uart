(function (module) {
	'use strict';
	
	module.generate = function (automaton, inputBits, outputBits, moduleName) {
		var initialStates = automaton.states.map(function (state) {return state.initial ? '1' : '0';}).reverse().join('');
		var finalStates = automaton.states.map(function (state) {return state.final ? '1' : '0';}).reverse().join('');
		
		var code = [];
		
		// header
		code.push(
			'library IEEE;',
			'use IEEE.STD_LOGIC_1164.all;',
			'use ieee.numeric_std.all;',
			'',
			'entity ' + moduleName + ' is',
			'	port(',
			'		reset: in std_logic;',
			'		shift_in: in std_logic;',
			'		data: in std_logic_vector(' + (inputBits - 1) + ' downto 0);',
			'		matched: out std_logic;',
			'		output: out std_logic_vector(' + (outputBits - 1) + ' downto 0) := (others => \'0\')',
			'	);',
			'end entity ' + moduleName + ';',
			''
		);
		
		
		// implementation header
		code.push(
			'architecture arch of ' + moduleName + ' is',
			'	constant zeroes: std_logic_vector(' + (automaton.states.length - 1) + ' downto 0) := (others => \'0\');',
			'	constant initial_states: std_logic_vector(' + (automaton.states.length - 1) + ' downto 0) := "'
				+ initialStates + '";',
			'	constant final_states: std_logic_vector(' + (automaton.states.length - 1) + ' downto 0) := "'
				+ finalStates + '";',
			'',
			'	signal states: std_logic_vector(' + (automaton.states.length - 1) + ' downto 0) := "' + initialStates + '";',
			'begin',
			'	matched <= \'0\' when ((states and final_states) = zeroes) or (reset = \'1\') else \'1\';',
			'',
			'	process(reset, shift_in)',
			//'		variable old_states: std_logic_vector(' + (automaton.states.length - 1) + ' downto 0);',
			'	begin',
			'		if reset = \'1\' then',
			'			states <= initial_states;',
			'		elsif shift_in\'event and shift_in = \'1\' then',
			//'			old_states := states;',
			'			states <= zeroes;',
			''
		);
		
		automaton.transitions.forEach(function (transition) {
			var source = automaton.states.indexOf(transition.from);
			var target = automaton.states.indexOf(transition.to);
			code.push(
				'			if states(' + source + ') = \'1\' and std_match(data, "' + transition.label + '") then',
				'				states(' + target + ') <= \'1\';'
			);
			if (transition.code !== undefined) {
				code.push(
					'				' + transition.code
				);
			}
			code.push(
				'			end if;',
				''
			);
			
		});
		
		code.push(
			'		end if;',
			'	end process;',
			'end architecture arch;',
			''
		);
		
		return code.join('\n');
	};
})(window.VHDLMatcher = window.VHDLMatcher || {});
