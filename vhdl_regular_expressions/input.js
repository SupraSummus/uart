/**
 * helpers
 */

var toBinary = function (number, length) {
	var binary = number.toString(2);
	var padding = Array(length - binary.length + 1).join('0');
	return '' + padding + binary;
};

var regexpString = function (string, code) {
	var regexp = Regexp.and(string.split('').map(function (ch) {
			return Regexp.leaf(toBinary(ch.charCodeAt(0), 8));
	}));
	regexp.elements[regexp.elements.length - 1].code = code;
	return regexp;
};


var anyChar = Regexp.leaf('--------');

/**
 * regexp definition
 */
var regexp = Regexp.and([
	Regexp.many(anyChar),
	Regexp.or([
		regexpString('hello', 'output(4 downto 3) <= "00";'),
		Regexp.and([
			regexpString('led '),
			Regexp.or([0, 1, 2, 3, 4, 5, 6, 7].map(function (i) {
				return regexpString(
					i.toString(10),
					'output(2 downto 0) <= "' + toBinary(i, 3) + '";'
				);
			})),
			regexpString(' '),
			Regexp.or([
				regexpString('toggle', 'output(4 downto 3) <= "01";'),
				regexpString('get', 'output(4 downto 3) <= "10";'),
			]),
		]),
		regexpString('buttons', 'output(4 downto 3) <= "11";'),
	]),
	Regexp.or([
		regexpString('\n'),
		regexpString('\r'),
	]),
]);
console.log('regexp', regexp);

/**
 * automaton
 */

var automaton = Automaton.fromRegexp(regexp);
console.log('automaton', automaton);

/**
 * vhdl code
 */
var code = VHDLMatcher.generate(automaton, 8, 5, 'matcher');
console.log('code', code);
document.getElementById('code').innerHTML = code;
