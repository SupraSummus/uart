(function (module) {
	'use strict';
	
	/**
	 * atomaton is an object with properties
	 *  * states - array of all state objects {initial, final, ...}
	 *  * transitions - array of objects {from, to, label, ...}
	 */
	module.empty = function () {
		return {
			states: [],
			transitions: [],
		};
	};
	
	module.state = function (automaton, initial, final) {
		var state = {
			initial: initial,
			final: final,
		};
		automaton.states.push(state);
		return state;
	};
	
	module.transition = function (automaton, from, to, label) {
		var transition = {
			from: from,
			to: to,
			label: label,
		};
		automaton.transitions.push(transition);
		return transition;
	};
	
	/**
	 * removes null transitions
	 */
	module.removeNullTransitions = function (automaton) {
		while (true) {
			// find one null transition
			var nullTransitions = automaton.transitions.filter(function (transition) {
				return transition.label === null;
			});
			if (nullTransitions.length === 0) {
				return automaton;
			}
			
			var target = nullTransitions[0].to;
			var source = nullTransitions[0].from;
			
			// remove transition
			automaton.transitions.splice(
				automaton.transitions.indexOf(nullTransitions[0]),
				1
			);
			
			// redirect transitions asociated with source
			automaton.transitions.filter(function (transition) {
				return transition.to === source;
			}).forEach(function (transition) {
				transition.to = target;
			});
			automaton.transitions.filter(function (transition) {
				return transition.from === source;
			}).forEach(function (transition) {
				transition.from = target;
			});
			
			// rewrite attributes
			target.initial = target.initial || source.initial;
			target.final = target.final || source.final;
			
			// remove source
			automaton.states.splice(
				automaton.states.indexOf(source),
				1
			);
		}
	};
	
	/**
	 * make automaton for given regexp
	 */
	module.fromRegexp = function (regexp) {
		var expand = function(automaton, regexp, initialState) {
			if (regexp.type === 'and') {
				regexp.elements.forEach(function (element) {
					initialState = expand(automaton, element, initialState);
				});
				return initialState;
				
			} else if (regexp.type === 'or') {
				var endState = module.state(automaton, false, false);
				regexp.elements.forEach(function (element) {
					module.transition(
						automaton,
						expand(automaton, element, initialState),
						endState,
						null
					);
				});
				return endState;
				
			} else if (regexp.type === 'many') {
				module.transition(
					automaton,
					expand(automaton, regexp.element, initialState),
					initialState,
					null
				);
				return initialState;
				
			} else if (regexp.type === 'leaf') {
				var endState = module.state(automaton, false, false);
				var transition = module.transition(
					automaton,
					initialState,
					endState,
					regexp.label
				);
				transition.code = regexp.code;
				return endState;
				
			} else {
				throw new Error('unknown node type ' + regularExpression.type);
			}
		};
		
		var automaton = module.empty();
		var initialState = module.state(automaton, true, false);
		var finalState = expand(automaton, regexp, initialState);
		finalState.final = true;
		
		automaton = module.removeNullTransitions(automaton);
		
		return automaton;
	};
})(window.Automaton = window.Automaton || {});
