(function (module) {
	'use strict';
	
	/**
	 * function for constructing regexps
	 */
	
	/**
	 * all of elements in a row
	 */
	module.and = function (elements) {
		return {
			type: 'and',
			elements: elements,
		};
	};
	
	/**
	 * at least one of elements
	 */
	module.or = function (elements) {
		return {
			type: 'or',
			elements: elements,
		};
	};
	
	/**
	 * 0 or more
	 */
	module.many = function (element) {
		return {
			type: 'many',
			element: element,
		};
	};
	
	/**
	 * single element
	 */
	module.leaf = function (label) {
		return {
			type: 'leaf',
			label: label,
		};
	};
})(window.Regexp = window.Regexp || {});
