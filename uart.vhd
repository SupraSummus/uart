library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity uart is
	generic(
		clock_frequency: natural;
		baud_rate: natural;
		nbit: natural := 8
	);
	port(
		reset: in std_logic;
		clock: in std_logic;
		
		input: in std_logic_vector(nbit - 1 downto 0);
		tx_strobe: in std_logic;
		tx_ready: out std_logic;
		comm_out: out std_logic;
		
		output: out std_logic_vector(nbit - 1 downto 0);
		rx_strobe: in std_logic;
		rx_ready: out std_logic;
		comm_in: in std_logic
	);
end entity uart;

architecture arch of uart is
begin
	receiver: entity work.universal_asynchronous_receiver
		generic map(clock_frequency, baud_rate, nbit)
		port map(reset, clock, output, rx_strobe, rx_ready, comm_in);
	
	transmitter: entity work.universal_asynchronous_transmitter
		generic map(clock_frequency, baud_rate, nbit)
		port map(reset, clock, input, tx_strobe, tx_ready, comm_out);
end architecture arch;
