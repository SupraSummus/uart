library IEEE;

package utils is
	function binary_length(x: natural) return natural;
end utils;

package body utils is

function binary_length(x: natural) return natural is
	variable temp : natural := x;
	variable n : natural := 1;
begin
	while temp > 1 loop
		temp := temp / 2;
		n := n + 1;
	end loop;
	return n;
end function binary_length;

end utils;