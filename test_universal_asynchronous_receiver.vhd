--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:	23:26:58 12/15/2014
-- Design Name:	
-- Module Name:	D:/win/Xilinx/workspace/zadanie3-serial/test_universal_asynchronous_receiver.vhd
-- Project Name:  zadanie3-serial
-- Target Device:  
-- Tool versions:  
-- Description:	
-- 
-- VHDL Test Bench Created by ISE for module: universal_asynchronous_receiver
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_universal_asynchronous_receiver IS
END test_universal_asynchronous_receiver;
 
ARCHITECTURE behavior OF test_universal_asynchronous_receiver IS 
 
	-- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT universal_asynchronous_receiver
		generic(
			clock_frequency: natural;
			baud_rate: natural;
			nbit: natural := 8
		);
		PORT(
			reset: in std_logic;
			clock : IN  std_logic;
			output : OUT  std_logic_vector(nbit - 1 downto 0);
			strobe : IN  std_logic;
			ready : OUT  std_logic;
			input : IN  std_logic
		);
	END COMPONENT;
	 

	--Inputs
	signal clock : std_logic := '0';
	signal strobe : std_logic := '0';
	signal input : std_logic := '0';

 	--Outputs
	signal output : std_logic_vector(4 downto 0);
	signal ready : std_logic;

	-- Clock period definitions
	constant clock_frequency: natural := 50000000;
	constant clock_period : time := 1000000000ns / clock_frequency;
	constant baud_rate: natural := 115200;
	constant baud_period: time := 1000000000ns / baud_rate;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
	uut: universal_asynchronous_receiver
		generic map(clock_frequency, baud_rate, 5)
		PORT MAP (
			'0',
			clock => clock,
			output => output,
			strobe => strobe,
			ready => ready,
			input => input
		);

	-- Clock process definitions
	clock_process :process
	begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
	end process;
 

	-- Stimulus process
	stim_proc: process
	begin
		input <= '1';
		wait for 50ns;
		
		strobe <= '1';
		if ready = '1' then
			wait until ready = '0';
		end if;
		strobe <= '0';
		
		input <= '0';
		wait for baud_period;
		input <= '0';
		wait for baud_period;
		input <= '1';
		wait for baud_period;
		input <= '0';
		wait for baud_period;
		input <= '0';
		wait for baud_period;
		input <= '1';
		wait for baud_period;
		input <= '1';
		assert ready = '0';
		wait for baud_period;
		
		wait for clock_period*10;
		assert ready = '1';
		assert output = "10010";
		
		strobe <= '1';
		wait for clock_period*10;
		assert ready = '0';
		
		wait;
	end process;

END;
